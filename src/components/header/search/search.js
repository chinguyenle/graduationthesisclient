
import React, { Component } from 'react';
import _ from 'lodash';

import 'antd/dist/antd.css';
import './search.scss';
import { Icon, Input, AutoComplete } from 'antd';
import { apiSearch } from '../../../api/api-search';
import { browserHistory } from '../../../common/history';

const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;

export class Search extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            classes: []
        }
    }

    getResult(keyWord) {
        apiSearch.search(keyWord)
            .then(res => {
                if(res.success){
                    this.setState({
                        users: res.users,
                        classes: res.classes
                    })
                }
            })
            .catch(e => console.log(e))
    }

    onChange=(value)=> {
        // console.log("e.target.value ", typeof value, value.length )
        if(value.length > 0)
        this.getResult({ keyWord: value.toString() })
        else {
            this.setState({
                users: [],
                classes: []
            })
        }
    }

    renderResults = () => {
        const { users, classes } = this.state;
        return [
            <OptGroup key={"Teacher"} label="Teacher">
                {users.map(user => (
                    <Option key={"user-"+user.id} value={user.username} >
                        {user.username +" - "+user.email}
                    </Option>
                ))}
            </OptGroup>
            ,
            <OptGroup key={"Class"} label="Class">
                {classes.map(classRoom => (
                    <Option key={"class-"+classRoom.id} value={classRoom.classRoom.classRoomName}>
                        {classRoom.classRoom.classRoomName +" - "+ classRoom.createdBy.name }
                    </Option>
                ))}
            </OptGroup>
        ]
    }

    render() {
        return (
            <div className="certain-category-search-wrapper" style={{ width: 250 }}>
                <AutoComplete

                    className="certain-category-search"
                    // dropdownClassName="certain-category-search-dropdown"
                    dropdownMatchSelectWidth={false}
                    dropdownStyle={{ width: 300 }}
                    size="default"
                    style={{ width: '100%' }}
                    dataSource={this.renderResults()}
                    placeholder="Teacher or classs"
                    optionLabelProp="value"
                    onSearch={(e)=> this.onChange(e)}
                    onSelect={(e, op) => {
                        // console.log(" select", e, op); 
                        browserHistory.replace(`/search=${e}&key=${op.key}`)
                    }}
                >
                    {/* <Input suffix={<Icon type="search" className="certain-category-icon" />} /> */}
                </AutoComplete>
            </div>
        );
    }
}