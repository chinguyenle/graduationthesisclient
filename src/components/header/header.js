import React, { Component } from 'react'
import _ from 'lodash'

import convertIcon from '../../assets/images/convert.png'
import HusConvert from '../../assets/images/HusConvert.png'
import './header.scss'
import { ButtonExtent } from './button-extent';
import { Search } from './search/search';
import { Avatar } from '../avatar/avatar';
import { browserHistory } from '../../common/history';
import { DropDownCus } from '../dropdown/drop-down';
import { openModalLogin } from '../../routes/guest-routes/login';
import { ROLE, API_BASE_URL } from '../../constants';
import { api } from '../../api/api';
import { getUrlFile } from '../../common/funtions';


export default class Header extends Component {

  renderHeaderRight() {
    const { user, headerRight } = this.props;
    let roleName = "";
    let avatarUrl = "";
    let name=""
    if (!_.isEmpty(user)) {
      const { avatarId } = user.user
      avatarUrl = avatarId?getUrlFile(avatarId):null;
      roleName = user.user.roles[0].name;
      name=user.user.name;
    }
    if(headerRight) return headerRight;
    switch (roleName) {
      case ROLE.ROLE_USER:
        return headerRightStudent(avatarUrl, name);
      case ROLE.ROLE_TEACHER:
        return headerRightTeacher(avatarUrl, name);
      default:
        return headerRightLogin();
    }
  }

  render() {
    const { headerMid, user } = this.props;
    let avatarUrl = '';
    if (user.user) {

      let { avatarId } = user.user
      avatarUrl = `${API_BASE_URL}file/fileId=${avatarId}`
      // api.get(`downloadFile/${avatarId}`).
      // then(res => console.log('res', res))
      // .catch(e => console.log('e', e))
    }
    return (
      <div className="header-container">
        <div className="main-header">
          <div className="header-logo" onClick={() => browserHistory.push("/welcome")}>
            <img src={convertIcon} style={{ height: 40, width: 'auto' }} alt="img" />
            <img src={HusConvert} alt="img" />
          </div>
          {user.user &&
            <div className="header-mid">
              <Search />
            </div>
          }
          {
            <div className="header-right">
              {this.renderHeaderRight()}
            </div>
          }
        </div>
      </div>
    );
  }
}

const headerRightTeacher = (avatarUrl, name) => (
  <div className="header-right-teacher">
    <ButtonExtent name="new class" onClick={() => browserHistory.push('/newclass')} />
    <ButtonExtent name="new lesson" onClick={() => browserHistory.push('/newlesson')} />
    <ButtonExtent name="my class" onClick={() => browserHistory.push('/myclass')} />
    <DropDownCus isTeacher>
      <Avatar iconDown avatar={avatarUrl} name={name} />
    </DropDownCus>
  </div>
)
const headerRightStudent = (avatarUrl, name) => (
  <div className="header-right-teacher">
    <ButtonExtent name="all classes" onClick={() => browserHistory.push('/allclasses')} />
    <ButtonExtent name="my classes" onClick={() => browserHistory.push('/myclass')} />
    <DropDownCus >
      <Avatar iconDown avatar={avatarUrl} name={name} />
    </DropDownCus>
  </div>
)

const headerRightLogin = () => (
  <div className="right-header-button">
    <div className="right-header-button-view">
      <ButtonExtent name="Over view" onClick={() => browserHistory.push('/welcome')} />
      <ButtonExtent name="Mision" onClick={() => browserHistory.push('/welcome')} />
      <ButtonExtent name="About us" onClick={() => browserHistory.push('/welcome')} />
    </div>
    <div className="right-header-button-login">
      <ButtonExtent name="Login" className="login" onClick={() => openModalLogin()} />
      <ButtonExtent name="Register" onClick={() => browserHistory.push('/register')} />
    </div>

  </div>)