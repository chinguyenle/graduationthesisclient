import React,  {Component} from 'react';
import classnames from 'classnames'


import './button-extent.scss'


export class ButtonExtent extends Component {
  render(){
    const {onClick, active, name, className, hover} = this.props;
    return(
      <div onClick={onClick} className= {classnames("button-extent", className, {effectHover: hover})} style={active?{backgroundColor: 'red'}:{}} >
        {name}
      </div>
    );
  }
}