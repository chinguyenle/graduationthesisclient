import React, {Component} from 'react';
import classnames from 'classnames'
import './header-types.scss'

const Types = [
    {
        name: "Tất cả",
        value: 'all'
    },
    {
        name: "Giáo dục",
        value: 'graducation'
    },
    {
        name: "Kinh tế",
        value: 'economic'
    },
    {
        name: "Khác",
        value: 'other'
    },
]
export class HeaderTypes extends Component {
    constructor(props) {
        super(props);
        this.state={
            typeActived:'all'
        }
    }

    activeType(typeName){
        this.setState({
            typeActived: typeName
        })
    }
    render() {
        let {typeActived} = this.state;
        return (
            <div className="header-types">
                {
                    Types.map(item => (
                        <div className={classnames("type",{active: item.value===typeActived} ) }onClick={()=>this.activeType(item.value)}>
                            {item.name}
                        </div>
                    ))
                }
            </div>
        )
    }
}