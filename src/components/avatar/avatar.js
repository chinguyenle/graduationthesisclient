import React, { Component } from 'react'
import classnames from 'classnames'
import KeyBoardArrowDown from '../../assets/icon/icon-keyboard-arrow-down.png'
import _ from 'lodash'
import './avatar.scss'
import { getAvatarColor } from '../../common/color';


export class Avatar extends Component {
  render() {
    const { iconDown, avatar, name, hasName, id, className } = this.props;
    return (
      <div className={classnames("avatar-container", className)}>
        {_.isEmpty(avatar) ? <div id={id} className="avatar avatar-character" style={{ backgroundColor: getAvatarColor(name)}}>
          {name[0].toUpperCase()}
        </div>
          :
          <img id={id} src={avatar} className="avatar" />
        }
        {hasName?<div className="avatar-name">{name}</div>:null}
        {iconDown && <img src={KeyBoardArrowDown} className="icon-down" />}
      </div>
    );
  }
}