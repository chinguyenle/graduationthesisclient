import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash';

import { Tooltip } from 'antd'
import './class-card.scss'
import ButtonBorder from '../button/button-border';
import { getUrlFile } from '../../common/funtions';
import swal from 'sweetalert';
import DefaultClassImage from '../../assets/images/defaultClassIcon.jpg'
import { apiClass } from '../../api/api-class';

class ClassCard extends Component {
  hasJoinedClass() {
    const { classInfo, user } = this.props;
    const { users, createdBy } = classInfo.classRoom;
    console.log('porps', this.props);
    if (user.id === createdBy) return 0;
    if (classInfo.sendRequestToJoin) return 1;
    let checkStudent = _.findIndex(users, oldUser => oldUser.id === user.id);
    if (checkStudent != -1) return 0;
    return 2;
  }

  joinedRequest(e) {
    const { callBack } = this.props;
    e.stopPropagation();
    apiClass.sendRequest({ classId: this.props.classInfo.id })
      .then(res => {
        if (res.success) {
          swal({
            title: "",
            text: "Your request is sent!",
            icon: "success",
          }).then(() => callBack())
        }
        console.log(res)
      })
      .catch(e => console.log(e))
  }

  render() {
    // console.log('posp', this.props)
    const { classInfo, onClick, showListRequest } = this.props;
    const { createdBy, listRequestToJoin } = classInfo
    const { avatarId, classRoomName, lessons, users } = classInfo.classRoom;
    return (
      <div
        className="class-card"
        onClick={() => this.hasJoinedClass() == 0
          ? onClick()
          : swal({
            title: "Oh no!",
            text: "You haven't joined this class yet!, Send request and wait for teacher's approval!",
            icon: "error",
          })}>
        <div className="class-avatar">
          <img className="avatar" src={avatarId ? getUrlFile(avatarId) : DefaultClassImage} />
        </div>
        <div className="class-info">
          <div className="class-info-name class-name"><div>Class:</div> <span>{classRoomName}</span></div>
          <div className="class-info-name"><div>Teacher:</div> <span>{createdBy.name}</span></div>
          <div className="class-info-name"><div>Members:</div> <span>{users.length || 0}</span></div>
          <div className="class-info-name"><div>Lesson:</div> <span>{lessons.length || 0}</span></div>
        </div>
        {showListRequest&&listRequestToJoin && listRequestToJoin.length > 0 &&

          <Tooltip title={`${listRequestToJoin.length} requests to join`}>

            <div className="has-join-request"
              onClick={(e) => {
                e.stopPropagation();
                showListRequest(listRequestToJoin)
              }}>
              {listRequestToJoin.length}

            </div>
          </Tooltip>}
        {this.hasJoinedClass() == 2
          ?
          <ButtonBorder
            className="button-join"
            content={<div>&#43; Join</div>}
            onClick={(e) => this.joinedRequest(e)}
          />
          : this.hasJoinedClass() == 1
            ? <ButtonBorder
              className="button-join"
              content={<div>&#43; Pending</div>}
              onClick={(e) => { e.stopPropagation() }}
            />
            : null
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(ClassCard);