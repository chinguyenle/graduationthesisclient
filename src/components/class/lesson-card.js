import React, { Component } from 'react';
import { connect } from 'react-redux'

import classnames from 'classnames'
import { Avatar } from '../avatar/avatar';
import moment from 'moment';
import { Love, Comment } from '../slide/reaction/reaction';

import './lesson-card.scss'
import { getUrlFile } from '../../common/funtions';
import { Popover } from 'antd';
import { ButtonExtent } from '../header/button-extent';
import { apiLesson } from '../../api/api-lesson';
import { browserHistory } from '../../common/history';
import swal from 'sweetalert';

class LessonCard extends Component {
    constructor(props) {
        super(props);
    }

    setActiveLike() {
        const { lessonInfo } = this.props;
        const { user } = this.props;
        let activeLike = false;
        if (lessonInfo.likes.length > 0) {
            lessonInfo.likes.map(item => {
                if (item.id === user.id) activeLike = true;
            })
        }
        return activeLike;
    }

    editLesson(lesson) {
        apiLesson.getLessonDetail(lesson.id)
            .then(res => {
                console.log('lesson detail', res)
                browserHistory.push("/edit", { newLesson: res.lesson })
            })
            .catch(e => console.log(e))
    }

    deleteLesson(lesson) {
        const {refreshData} = this.props;
        console.log('lesson.id ', lesson.id)
          swal({
            title: 'Delete this lesson!!!',
            icon: "warning",
          }).then((value) => {
            console.log('value', value);
            apiLesson.deleteLesson(lesson.id)
            .then(res => {
                if(res.success)
                swal({
                    title: "Lesson is deleted",
                    icon: "success",
                  })
                  refreshData();
            })
            .catch(e => console.log(e))
          })
       
    }

    render() {
        const { lessonInfo, onClick, className } = this.props;
        const { name, avatarId, type, likes, comments, auth, createdAt, isLoved, createdBy } = lessonInfo
        let activeLike = this.setActiveLike();
        return (
            <Popover placement="leftTop" content={
                <div>
                    <ButtonExtent hover name={"Edit"} onClick={() => this.editLesson(lessonInfo)} className="button-popover" />
                    <ButtonExtent hover name={"Delete"} onClick={() => this.deleteLesson(lessonInfo)} />
                </div>
            }>
                <div className={classnames("lesson-card", className)} onClick={onClick}>
                    <div
                        className="lesson-card-header"
                        style={{
                            backgroundImage: `url(${getUrlFile(avatarId)})`,
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            // filter:' blur(0.8px)'
                        }}>
                        {/* <img className="lesson-card-image" alt="card" src={lessonAvatar} /> */}
                        <div className="blur-background type">{type ? type : "Khoa học, Giáo dục"}</div>
                        <div className="blur-background lesson-name">{name}</div>
                    </div>
                    <div className="interactions">
                        <Love active={activeLike} number={likes.length} onClick={(love) => console.log('love', love)} />
                        <Comment number={comments} />
                    </div>
                    <div className="lesson-card-footer">
                        <Avatar
                            avatar={getUrlFile(createdBy.avatarId)}
                            name={createdBy.name}
                            hasName
                        />
                        <div className="create-at">{moment(createdAt).format('DD/MM/YYYY')}</div>
                    </div>
                </div>
            </Popover>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

export default connect(mapStateToProps)(LessonCard)