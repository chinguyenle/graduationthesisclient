import React, { Component } from 'react'


import './control-tabbar.scss'
import { ButtonIcon } from '../../button/button-icon';
import { IconName } from '../../../common/icon-name';
import { Slider } from 'antd';
import moment from 'moment';


export default class ControlTabbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      play: props.play || false,
      mute: false,
      volume: 50,
      showComment: true
    }
  }

  onChange(key, value) {
    const { setPlay } = this.props;
    this.setState({
      [key]: value
    }, () => setPlay(value))
  }

  setShowComment(value) {
    const { showComment } = this.props;
    this.setState({
      showComment: value
    }, () => showComment(value))
  }
  changeVolume(value) {
    const {changeVolume} = this.props;
    if (value === 0) {
      this.setState({
        mute: true,
        volume: false
      })
    } else {
      this.setState({
        mute: false,
        volume: value
      }, () => changeVolume(value/100))
    }
  }

  formatTimeToMinSe(seconds) {
    return moment.unix(seconds).format('mm:ss')
  }
  render() {
    let { play, mute, volume, showComment } = this.state;
    let { timeplayed, totalTime, handleReplay } = this.props;
    return (
      <div className="control-tabbar">
        <div className="play-pause">
          {play
            ? timeplayed === totalTime ?
              <ButtonIcon
                iconName={IconName.Replay}
                onClick={() => handleReplay()}
              />
              : <ButtonIcon
                iconName={IconName.Pause}
                onClick={() => this.onChange('play', false)}
              />
            :
            <ButtonIcon
              iconName={IconName.Play}
              onClick={() => this.onChange('play', true)}
            />
          }
        </div>
        <div className="volume" >
          {!mute
            ?
            volume > 50
              ? <ButtonIcon
                iconName={IconName.VolumeUp}
                onClick={() => { this.changeVolume(0) }}
              />
              : <ButtonIcon
                iconName={IconName.VolumeDown}
                onClick={() => { this.changeVolume(0) }}
              />
            :
            <ButtonIcon
              iconName={IconName.VolumeOff}
              onClick={() => { this.changeVolume(20) }}
            />
          }
          <Slider
            className="slider"
            value={volume}
            onChange={value => this.changeVolume(value)}
            min={0}
            max={100}
          />
        </div>
        <div className="time">
          {this.formatTimeToMinSe(timeplayed>0?timeplayed:0)}-{this.formatTimeToMinSe(totalTime)}
        </div>

        <div className="comment">
          {
            showComment ?
              <ButtonIcon
                iconName={IconName.CommentShow}
                onClick={() => { this.setShowComment(false) }}

              />
              : <ButtonIcon
                iconName={IconName.CommentHidden}
                onClick={() => { this.setShowComment(true) }}
              />
          }
        </div>
      </div>
    );
  }
}