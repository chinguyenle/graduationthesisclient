import React, { Component } from 'react'
import classnames from 'classnames'

import './slide-show.scss'
import ControlTabbar from './controll/control-tabbar';
import { getUrlFile } from '../../common/funtions';


export class SlideShow extends Component {

  constructor(props) {
    super(props);
    // console.log('props slide', props)
    const { data } = this.props;
    this.totalTime = data.total_Time;
    this.initState = {
      play: false,
      showComment: true,
      indexSlide: 0,
      slide: data.slides[0],
      sub: {},
      timeplayed: -1
    }
    this.state = {
      ...this.initState
    }
  }
  componentDidMount() {
    this.audio.volume=0.5
    this.showSlide();
  }
  showSlide() {
    const { data } = this.props;
    this.countdown = setInterval(() => {
      let { play, timeplayed, slide, indexSlide, sub } = this.state;
      let timeplayed_temp = timeplayed + 1;
      if (play && timeplayed_temp <= this.totalTime) {
        if (timeplayed_temp > slide.endTime && indexSlide < data.slides.length - 1) {
          return this.setState({
            slide: data.slides[indexSlide + 1],
            indexSlide: indexSlide + 1,
            timeplayed: timeplayed_temp,
            sub: data.slides[indexSlide + 1].notes.length > 0 && data.slides[indexSlide + 1].notes[0].startTime === timeplayed ? data.slides[indexSlide + 1].notes[0] : {}
          })
        }
        slide.notes.map(item => {
          if (timeplayed_temp <= item.endTime && timeplayed_temp >= item.startTime) {
            if (item.id !== sub.id)
              this.setState({
                sub: item
              }
              )
          }
        })
        this.setState({
          timeplayed: timeplayed_temp
        })
      }
      if (timeplayed > this.totalTime) {
        this.setState({ play: false })
        clearInterval(this.countdown)
      }

    }, 1000);
  }
  handleReplay() {
    let { showComment } = this.state;
    clearInterval(this.countdown);
    this.setState({
      ...this.initState, play: true, showComment: showComment
    }, () => this.showSlide())

  }
  componentWillMount() {
    clearInterval(this.countdown)
  }

  componentDidUpdate(preProp, prevState) {
    let { play, timeplayed, slide, indexSlide, sub } = this.state;
    // console.log('play', play,prevState.play)
    if (play !== prevState.play)
      if (!play) this.audio.pause()
      else this.audio.play()
    if (sub.id && sub.id !== prevState.sub.id) {
      this.audio.src = sub.srcAudio;
      this.audio.pause()
      this.audio.load()
      this.audio.play()
    }
  }
  render() {
    const { slide, sub, showComment, timeplayed } = this.state;
    // console.log('sub', sub)
    return (
      <div className="slide-show-container">
        <div className="slide-show">
          <img src={getUrlFile(slide.srcSlide)} className="slide" />

          <div className={classnames("subtitle", { "hidden-comment": !showComment })}>
            <span>{sub.text}</span>
          </div>

          <div className="overlay-pause"></div>
        </div>
        <div className="controll-slide">
          <ControlTabbar
            setPlay={(value) => this.setState({ play: value })}
            handleReplay={() => this.handleReplay()}
            showComment={(value) => this.setState({ showComment: value })}
            timeplayed={timeplayed}
            totalTime={this.totalTime}
            changeVolume={(value) => this.audio.volume = value}
          />
        </div>
        <audio ref={ref => this.audio = ref}>
          <source src={sub.srcAudio} type="audio/mpeg" />
        </audio>
      </div>
    );
  }
}