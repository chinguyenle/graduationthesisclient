import React, { Component } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import PropTypes from 'prop-types'


import './layout.scss'
import Header from '../header/header';
import { logout } from '../../common/funtions';


class Layout extends Component {
  render() {
    const { children, headerMid, headerRight, className, titleContent, user, rightContent, leftContent } = this.props;
    // logout();
    return (
      <div className={classnames("layout-container", className)}>
        <Header headerMid={headerMid} headerRight={headerRight} user={user} />
        <div className="layout-content">
          {titleContent && <div className="title-content">{titleContent}</div>}
          <div className="main-content">

            <div className="left-content">{leftContent}</div>
            <div className="child-content">{children}</div>
            <div className="right-content">{rightContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

Layout.propsTypes = {
  children: PropTypes.element.isRequired,
  headerMid: PropTypes.element,
  headerRight: PropTypes.element,
  className: PropTypes.string,
  titleContent: PropTypes.string
}
const mapStateToProps = (state) => ({
  user: state.user,
})

export default connect(mapStateToProps)(Layout);
