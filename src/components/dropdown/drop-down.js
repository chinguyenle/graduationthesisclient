import React, { Component } from 'react'
import { Dropdown, Menu } from 'antd'

import './drop-down.scss'
import { browserHistory } from '../../common/history';
import { logout } from '../../common/funtions';


export class DropDownCus extends Component {
  render() {
    const { menu, children, logout, myLession, account, isTeacher } = this.props;
    return (
      <Dropdown overlay={menu ? menu : exampleMenu({ logout, myLession, account, isTeacher })} trigger={['click']} placement="bottomLeft">
        <div>
          {children}
        </div>
      </Dropdown>
    );
  }
}

const exampleMenu = ({ account, isTeacher }) => (
  <Menu>
    <Menu.Item key={0} onClick={() => browserHistory.push('/myaccount')}>
      Account
    </Menu.Item>
    {
      isTeacher
        ?
        <Menu.Item key={1} onClick={() => browserHistory.push('/mylesson')}>
          My lesson
    </Menu.Item>
        : null
    }
    <Menu.Item key={2} onClick={() => logout()}>
      Log out
    </Menu.Item>
  </Menu>
)

