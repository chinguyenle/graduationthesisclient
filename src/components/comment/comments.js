import React, { Component } from 'react'


import './comments.scss'
import { CommentUser } from './comment-user';
import { Spin } from 'antd'
import ButtonBorder from '../button/button-border';


var stompClient = null;
export default class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",

      // socket
      username: '',
      channelConnected: false,
      chatMessage: '',
      roomNotification: [],
      error: '',
      bottom: false,
      curTime: '',
      openNotifications: false,
      bellRing: false
      // socket
    }
  }

  changeInput(e) {
    this.setState({
      comment: e.target.value
    })
  }

  // socket
  connect = (userName) => {

    if (userName) {

      this.setState({
        username: userName
      })

      const Stomp = require('stompjs')

      var SockJS = require('sockjs-client')

      SockJS = new SockJS('http://localhost:5000/ws')

      stompClient = Stomp.over(SockJS);

      stompClient.connect({}, this.onConnected, this.onError);

    }
  }

  onConnected = () => {

    this.setState({
      channelConnected: true
    })

    // Subscribing to the public topic
    stompClient.subscribe('/topic/public', this.onMessageReceived);

    // Registering user to server
    stompClient.send("/app/addUser",
      {},
      JSON.stringify({ sender: this.state.username, type: 'JOIN' })
    )
  }

  onError = (error) => {
    console.log('errrorr comment', error)
    this.setState({
      error: 'Could not connect you to the Chat Room Server. Please refresh this page and try again!'
    })
  }

  sendMessage = (type, value) => {

    console.log('chat alue', value)
    if (stompClient) {
      var chatMessage = {
        sender: this.state.username,
        content: type === 'TYPING' ? value : value,
        type: type,
        localType: 'COMMENT'

      };

      stompClient.send("/app/sendMessage", {}, JSON.stringify(chatMessage));

      // clear message text box after sending the message

    }
  }

  onMessageReceived = (payload) => {

    var message = JSON.parse(payload.body);

    console.log('messageee ', message);
    if (message.type === 'JOIN') {
      // console.log("join")
    }
    else if (message.type === 'CHAT'&&message.localType==='COMMENT') {
      const {getComment } = this.props;
      // console.log("chat")
      getComment();
    }
    else if (message.type === 'LEAVE') {
      // console.log("leave")
    }
    else if (message.type === 'TYPING') {
      // console.log("typing")
    }
    else {
      // console.log("do nothing")
      // do nothing...
    }
  }

  fetchHostory = () => {
    alert('History Not Available!\nIt is Not Yet Implemented!');
  }

  scrollToBottom = () => {
    var object = this.refs.messageBox;
    if (object)
      object.scrollTop = object.scrollHeight;
  }

  componentDidMount(){
    const {user} = this.props;
    console.log(user)
    this.connect(user.user.username)
  }

  // socket
  render() {
    const { comments, postComment } = this.props;
    console.log("comment", comments)
    return (
      <div className="comment-container">
        <div className="title">{"Comments"}</div>
        <div className="comment-content">
          <div style={{ marginBottom: 20 }}>

            {/* <input className="input-comment" onChange={e => this.changeInput(e)}>

            </input> */}

            <textarea value={this.state.comment} rows={3} style={{ width: '100%', resize: 'none' }} placeholder="your comment" onChange={e => this.changeInput(e)} >

            </textarea>
            <ButtonBorder content="send" onClick={() => { 
              postComment(this.state.comment,()=> this.sendMessage("CHAT",this.state.comment) ); 
              this.setState({ comment: "" });
              }} />
          </div>
          {comments
            ? comments.map((item, key) => (
              <CommentUser
                key={key}
                comment={item}
              />
            ))
            : <Spin />}
        </div>
      </div>
    );
  }
}