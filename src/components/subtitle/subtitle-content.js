import React, { Component } from 'react'
import { Icon, InputNumber } from 'antd'
import _ from 'lodash'

import SpeakerIcon from '../../assets/icon/icon-speaker.png'
import ThreeIcon from '../../assets/icon/three-dots.png'
import './subtitle-content.scss'
import TextArea from 'antd/lib/input/TextArea';


export default class SubtitleContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      subtitle: { ...props.subtitle }
    }
  }
  componentWillReceiveProps(nextProps) {
    console.log('next poprs', this.props != nextProps);
    if (this.props != nextProps) {
      this.setState({
        subtitle: { ...nextProps.subtitle }
      })
    }
  }

  calculateTimeToTalk=(text: String)=> {
    if (text.trim().length > 0) {
      // console.log(" time", Math.ceil(text.split(" ").length * 60 / 250))
      return Math.ceil(text.split(" ").length * 60 / 250)
    }
    return 0
  }
  changeSubtitle(key, value: String) {
    let { subtitle } = this.state;
    const { onChangeSub, index } = this.props
    subtitle[key] = value;

    if ((subtitle.startTime + this.calculateTimeToTalk(value.trim())) > subtitle.endTime)
      subtitle.endTime = subtitle.startTime + this.calculateTimeToTalk(value)

    this.setState({
      subtitle: subtitle
    }, () => onChangeSub(index, subtitle))
  }

  changeStartTime(value) {

    if (_.isNumber(value)) {
      console.log(value);
      let { subtitle } = this.state;
      const { onChangeSub, index } = this.props
      let distanceStartToEnnd = subtitle.endTime - subtitle.startTime;
      
        subtitle.startTime = value;
        subtitle.endTime = subtitle.startTime + distanceStartToEnnd

      this.setState({
        subtitle: subtitle
      }, () => onChangeSub(index, subtitle))
    }
  }
  changeEndTime(value) {
    if (_.isNumber(value)) {
      console.log(value);
      let { subtitle } = this.state;
      const { onChangeSub, index } = this.props
        subtitle.endTime = value

      this.setState({
        subtitle: subtitle
      }, () => onChangeSub(index, subtitle))
    }
  }
  deleteSub(){
    const {  deleteSub, onlyOneSub } = this.props
    if(onlyOneSub) {
      this.changeSubtitle("text", "")
    }else {
      deleteSub();
    }

  }
  render() {
    const { subtitle } = this.state;
    const { openSetting, deleteSub, minValueNote } = this.props
    // console.log('sub---', subtitle);
    // console.log('sub-props-', subtitle);
    return (
      <div className="subtitle-content">
        <Icon type="close" className="delete-sub" onClick={()=>this.deleteSub()} />
        <TextArea className="input" value={subtitle.text} onChange={e => this.changeSubtitle('text', e.target.value)} />
        <div className="time">
          <InputNumber key={"start"+subtitle.id} className="input-time" key={Math.random()} min={minValueNote} onChange={(value) => this.changeStartTime(value)} value={subtitle.startTime} />
         <InputNumber key={"end"+subtitle.id} className="input-time" key={Math.random()} min={subtitle.startTime + this.calculateTimeToTalk(subtitle.text.trim())} onChange={(value) => this.changeEndTime(value)} value={subtitle.endTime} defaultValue={subtitle.endTime} />
        </div>
        {/* <div className="speaker icon" onClick={() => console.log('speaker')}>
          <img src={SpeakerIcon} alt="speaker" />
        </div> */}
        <div className="icon" onClick={() => openSetting()}>
          <img src={ThreeIcon} alt="three dots" />
        </div>
      </div>
    );
  }
}