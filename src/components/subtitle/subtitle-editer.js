import React,  {Component} from 'react'
import _ from 'lodash';
import AddIcon from '../../assets/icon/icon-add.png';
import './subtitle-editer.scss'
import SubtitleContent from './subtitle-content';
import { openSettingModal } from '../modal/modal-edit-sub';

export default class SubtitleEditer extends Component {
  constructor(props){
    super(props);
    this.state={
      subtitles: props.subtitles||[]
    }
  }

  addNewSubtitle(){
    const {onChange, minValueSlide, indexActivedSlide} = this.props;
    let {subtitles} = this.state;
    let lastIndex = subtitles.length - 1;
    let newTitle = {
      text:"", 
      id:null,
      readdingSpeed: "1.0",
      voiceType: "hn_male_xuantin_vdts_48k-hsmm",
      startTime: subtitles.length>0?subtitles[lastIndex].endTime+1:minValueSlide+1,
      endTime: subtitles.length>0?subtitles[lastIndex].endTime + 2:minValueSlide+2};
      console.log('new sub', newTitle)
    subtitles.push(newTitle);
    this.setState({
      subtitles
    }, onChange(indexActivedSlide, subtitles))
  }

  componentWillReceiveProps(nextProps){
    // if(nextProps.subtitles!= this.props.subtitles){
      this.setState({
        subtitles: nextProps.subtitles
      })
    // }
  }
  deleteSub(key){
    const {onChange, indexActivedSlide} = this.props;
    let {subtitles} = this.state;
    subtitles.splice(key, 1)
    this.setState({subtitles}, onChange(indexActivedSlide, subtitles) );
  }
  changeSub=(key, sub)=>{
    const {onChange, indexActivedSlide} = this.props;
    let newSubTitle = this.state.subtitles;
    newSubTitle[key] = sub;
    // if(key < (newSubTitle.length -1)) {
      for (let index = (key+1); index < newSubTitle.length; index++) {
        // console.log('index ', index)
        let durationTime = newSubTitle[index].endTime - newSubTitle[index].startTime;
        if(newSubTitle[index].startTime<=newSubTitle[index-1].endTime){
          newSubTitle[index].startTime = newSubTitle[index-1].endTime + 1;
          newSubTitle[index].endTime = newSubTitle[index].startTime + durationTime;
        }
        
      }
    // }s
    // console.log('new subtitle', key,key < (newSubTitle.length -1))
    this.setState({
      subtitles:newSubTitle,
    }, onChange(indexActivedSlide, newSubTitle))
  }
  render(){
    let { name, minValueSlide } = this.props;
    let { subtitles } = this.state;
    // console.log('subbtitle', subtitles)
    return(
      <div className="subtitle-container">
        <div className="title">{name}</div>
        <div className="list-subtitle">
          {
            subtitles.map((sub,key)=>(
              <SubtitleContent
                key={key}
                onlyOneSub={subtitles.length===1}
                subtitle={sub}
                openSetting={() => openSettingModal(key, sub, this.changeSub)}
                index={key}
                onChangeSub={this.changeSub}
                deleteSub={()=>this.deleteSub(key)}
                minValueNote={key ===0 ?minValueSlide: subtitles[key-1].endTime}
              />
            ))
          }
          <div className="add-subtitle">
            <img src={AddIcon} onClick={()=> this.addNewSubtitle() }/>
          </div>
        </div>
      </div>
    );
  }
}