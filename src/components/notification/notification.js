import React,  {Component} from 'react'
import moment from 'moment'

import './notification.scss'


export class Notification extends Component {
  render(){
    const {name, createdAt, content} = this.props;
    return(
      <div className="noti-container">
        <div className="creator-name">{name} <span className="time">{moment.utc(createdAt).local().format('HH:mm - L')}</span></div>
        <div className="noti-content">
          {content}
        </div>
      </div>
    );
  }
}