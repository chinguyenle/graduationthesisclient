import React, { Component } from 'react'


import './select-user.scss'
import { Select } from 'antd';
import ButtonBorder from '../button/button-border';


export class SelectUser extends Component {
  state = {
    options: []
  }

  handleChange=(value)=> {
    console.log('handle ', value);
    this.setState({
      options: value
    })
  }
  onClick(){
    const { onAdd } = this.props;
    const {options} = this.state;

    if(options.length>0) {
      onAdd(options, ()=> this.setState({options:[]}))
    }
  }
  render() {
    const { listUsers, title, buttonTitle } = this.props;
    let {options} = this.state;
    return (
      <div className="addUser">
        <div className="add-new-text">
          {title?title:"Add new member"}
          <ButtonBorder content={buttonTitle? buttonTitle:"Add"} className="add-user-button" onClick={()=>this.onClick()} />
        </div>
        <Select
          mode="multiple"
          size="large"
          value={options}
          style={{ width: '100%' }}
          placeholder="Please select"
          onChange={this.handleChange}
        >
          {listUsers.map(item => <Select.Option key={item.id}>{item.email}</Select.Option>)}
        </Select>
      </div>
    );
  }
}