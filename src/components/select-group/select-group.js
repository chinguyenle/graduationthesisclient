import React, { Component } from 'react'


import './select-group.scss'
import { Select } from 'antd';
import ButtonBorder from '../button/button-border';


export class SelectUserGroup extends Component {
  state={
    option:[]
  }

  handleChange(value){
    console.log('handle ', value);
  }
  render() {
    const {options, addUser} = this.props;
    return (
        <Select
          mode="multiple"
          size="large"
          style={{ width: '100%' }}
          placeholder="Please select"
          onChange={this.handleChange}
        >
          {options.map(item =><Select.Option key={item.id}>{item.email}</Select.Option>)}
        </Select>
    );
  }
}