import { api } from "./api";

export const apiLesson={
  initLesson: ({pptFile}) => api.postFormData('lesson/initLesson', {pptFile}),
  createLesson: (lesson) => api.post('lesson/createLesson',{...lesson}),
  editLesson: (lesson) => api.post('lesson/editLesson',{...lesson}),
  previewLesson: (lesson) => api.post('lesson/previewLesson',{...lesson}),

  deleteLesson: (lessonId) => api.post(`lesson/delete/id=${lessonId}`),

  getLessonByUser: (userId) => api.get(`lesson/user=${userId}`),
  getLessonDetail: (lessonId) => api.get(`lesson/id=${lessonId}`),

  like: ({userId, lessonId}) => api.postFormData('lesson/like', {userId, lessonId}),
  dislike: ({userId, lessonId}) => api.postFormData('lesson/dislike', {userId, lessonId}),

  postComment: ({userId, lessonId, text}) => api.postFormData('lesson/postComment', {userId, lessonId, text}),
  getComment: ({lessonId}) => api.postFormData('lesson/getComment', {lessonId}),

  getLessonNotInClass: (classId)=>api.get(`lesson/notInClass=${classId}`),

}