import { api } from "./api";

export const apiClass = {
  
  getAllClasses:() => api.get("classes/allClasses"),

  createClass: ({ className, avatar }) => api.postFormData('classes', { className, avatar }),

  getMyClasses: () => api.get("classes/myClass"),

  getUserNotInClass: (classId) => api.get(`classes/userNotIn-classId=${classId}`),

  getClassById: (classId) => api.get(`classes/${classId}`),

  addUserToClass: (classId, listUser) =>
    api.post(`classes/addStudent-classId=${classId}`, { listUser }),

  deleteUser: (classId, listUser) =>
    api.post(`classes/deleteStudent-classId=${classId}`, { listUser }),

  
  addLessonToClass: ({classId, lessonId}) => api.postFormData('classes/addLessonToClass', {classId, lessonId}),
  deleteLessonToClass: ({classId, lessonId}) => api.postFormData('classes/deleteLessonToClass', {classId, lessonId}),

  createNotification: ({classId, userId, text})=> api.postFormData('notification/createNofitication',{classId, userId, text}),
  getNotification: (classId) => api.get(`notification/classId=${classId}`),

  sendRequest: (classId) => api.postFormData('classes/sendJoinRequest', classId),
  acceptRequest: ({requestId, classId, userId}) => api.postFormData('classes/acceptJoinRequest',{requestId, classId, userId}),
  cancelRequest: (requestId) => api.postFormData('classes/cancelJoinRequest', requestId)
}