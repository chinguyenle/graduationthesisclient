import qs from 'querystringify'
import _ from 'lodash'

import { API_BASE_URL, POLL_LIST_SIZE, ACCESS_TOKEN } from '../constants'
import { lcStorage } from '../common/util';

let DEFAULT_HEADERS = {
  'Content-Type': 'application/json'
}

const MULTIPART_FORM_HEADER = {
  'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
  'Mime-Type': 'jpg|jpeg|png'
}
let headers = {
  default: () => {
    if (lcStorage.getAccessToken())
    {
       DEFAULT_HEADERS.Authorization='Bearer ' + localStorage.getItem(ACCESS_TOKEN)
    }
      return DEFAULT_HEADERS
  },
  multipart: () => {
    if (lcStorage.getAccessToken())
      return {'Authorization': 'Bearer ' + localStorage.getItem(ACCESS_TOKEN)}
       
  },
}
const baseUrl = API_BASE_URL;

export const api = {
  get: (endpoint, params) => {
    const options = {
      method: 'GET',
      headers: {
        ...headers.default()
      }
    }
    return fetch(baseUrl + endpoint + qs.stringify(params, true), options).then(
      result => {
        return result.json()
      }
    )
  },

  post: (endpoint, params) => {
    const options = {
      method: 'POST',
      body: JSON.stringify(params),
      headers: {
        ...headers.default()
      }
    }
    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  },

  postImage: (endpoint, params) => {
    let formData = new FormData()
    _.forIn(params, (value, key) => {
      // if (value !== null && value !== null) {
      formData.append(key, value)
      // }
    })
    let options = {
      method: 'POST',
      body: formData,
      headers: {
        ...headers.multipart()
      }
    }
    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  },

  postRaw: (endpoint, params) => {
    const options = {
      method: 'POST',
      body: JSON.stringify(params),
      headers: {
        ...headers.default()
      }
    }
    console.log(options)
    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  },

  put: (endpoint, params) => {
    const options = {
      method: 'PUT',
      body: JSON.stringify(params),
      headers: {
        ...DEFAULT_HEADERS,
        'Content-Type': 'application/json'
      }
    }
    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  },

  postFormData: (endpoint, params) => {
    console.log('header', headers.multipart())
    let formData = new FormData();
    _.forIn(params, (value, key) => {
      // if (value !== null) {
      formData.append(key, value);
      // }
    });
    const options = {
      method: 'POST',
      body: formData,
      headers: {
        ...headers.multipart()
      }
    }

    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  },

  postUrlFormEncoded: (
    endpoint,
    params,
    extraConfig
  ) => {
    const options = {
      method: 'POST',
      headers: {
        ...DEFAULT_HEADERS,
        ...extraConfig.headers
      },
      body: qs.stringify(params)
    }
    return fetch(baseUrl + endpoint, options).then(result => {
      return result.json()
    })
  }
}
