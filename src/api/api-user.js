import { api } from "./api";

export const apiUser = {
  getProfile: () => api.get("user/me"),
  getProfilerById: (id) => api.get(`user/id=${id}`),
  editUser: ({userId, name, email, newPassword, dateOfBirth, avatar, role }) =>
      api.postFormData(`user/edit`, { userId, name, email, newPassword, dateOfBirth, avatar, role }),
  
  checkCurrentPass: ({currentPass, userId}) => api.postFormData(`user/checkpassword`, {currentPass, userId} )

}