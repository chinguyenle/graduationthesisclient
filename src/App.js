import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react'

import { store, persistor } from './redux/configureStore'
import './App.scss';
import { browserHistory } from './common/history';
import PrivateRoute from './private-route';
import MyClass from './routes/author/my-class/my-class';
import Lesson from './routes/author/my-class/lesson/lesson';
import ClassDetail from './routes/author/my-class/class-detail/class-detail';
import { Register } from './routes/guest-routes/register';
import { lcStorage } from './common/util';
import { NewClass } from './routes/author/new-class';
import AllClass from './routes/author/all-class';
import { Welcome } from './routes/guest-routes/welcome/welcome';
import Edit from './routes/author/lesson/edit/edit';
import Upload from './routes/author/lesson/upload/upload';
import Dowload from './routes/author/lesson/dowload/dowload';
import myLesson from './routes/author/my-lesson/my-lesson';
import MyAccount from './routes/author/account/my-account';
import SearchResult from './routes/author/search/search-result';
// import AnimatedShootingStar from './components/animated/animated-shooting-start';

class App extends Component {
  componentDidMount() {
    console.log('tolen 0', lcStorage.getAccessToken());

  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router history={browserHistory}>
            <div className="container">
              <div className="content">
                <Switch>
                <Route exact path="/" render={() => (<Redirect to="/welcome" />)} />
                <Route path="/welcome" component={Welcome} />
                <Route path="/register" component={Register} />
                <Route path="/newlesson" component={Upload} />
                <Route path="/edit" component={Edit} />
                <Route exact path="/dowload" component={Dowload} />
                <PrivateRoute path="/allclasses" component={AllClass} />
                <PrivateRoute exact path="/search=:seach&key=:key" component={SearchResult} />
                <PrivateRoute path="/myaccount" component={MyAccount} />
                <PrivateRoute path="/newclass" component={NewClass} />
                </Switch>
                <Switch>
                  <PrivateRoute exact path="/myclass" component={MyClass} />
                  <PrivateRoute exact path="/myclass/:classId&:className" component={ClassDetail} />
                  <PrivateRoute exact path="/myclass/:classId&:className/lesson=:lesson" component={Lesson} />
                </Switch>
                <Switch>
                  <PrivateRoute exact path="/mylesson" component={myLesson} />
                  <PrivateRoute exact path="/mylesson/lesson=:lesson" component={Lesson} />
                </Switch>
              </div>
            </div>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
