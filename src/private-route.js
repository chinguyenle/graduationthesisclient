import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash';


class PrivateRoute extends Component {
  render() {
    let {component: Component, ...otherProps} = this.props;
        // const {landscapeMode} = this.state;

        // if(landscapeMode) return <LandscapeScreen/>;
        console.log('this props',this.props.user,  !_.isEmpty(this.props.user))
        return (
            <Route {...otherProps} render={props => (
                !_.isEmpty(this.props.user) ? (
                    <Component {...props}/>
                ) : (
                    <Redirect to={{
                        pathname: '/welcome',
                    }}/>
                )
            )}/>
        )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(PrivateRoute);