import React, { Component } from 'react'
import { connect } from 'react-redux';
import {Prompt } from 'react-router-dom'

import swal from 'sweetalert';
import convertIcon from '../../../../assets/images/convert.png'
import './edit.scss'
import { SlideImage } from '../../../../components/slide/slide-image';
import SubtitleEditer from '../../../../components/subtitle/subtitle-editer';
import Layout from '../../../../components/layout/layout'
import { browserHistory } from '../../../../common/history';
import { getUrlFile } from '../../../../common/funtions';
import { apiLesson } from '../../../../api/api-lesson';
import { store } from '../../../../redux/configureStore';
import { updateLesson } from '../../../../redux/actions/lesson-actions';
import { Spin, Button, Icon } from 'antd';
import { PreviewLessonModal } from './modal-preview';

class Edit extends Component {
  constructor(props) {
    super(props);
    const { newLesson } = props.location.state;
    let lesson = newLesson ? newLesson : props.lesson;
    this.isEdit = lesson.id != null;
    // console.log('props', lesson);
    this.state = {
      loading: false,
      slideActive: lesson.slides[0],
      keyActive: 1,
      lesson: lesson,
      showPrivewModal: false,
      unsaved: true,
      shouldBlockNavigation: true
    }
  }

  componentDidUpdate = () => {
    const {shouldBlockNavigation} = this.state;
    console.log('show', shouldBlockNavigation)
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => {
        console.log("href", window.location)
      }
    } else {
      window.onbeforeunload = undefined
    }
  }
  createLesson() {
    const { lesson } = this.state;
    console.log('lesson', lesson);
    this.setState({
      loading: true
    })
    if (this.isEdit) {
      apiLesson.editLesson(lesson)
        .then(res => {
          this.setState({
            loading: false,
            shouldBlockNavigation:false
          })
          console.log('res edit', res);
          if (res.success) {
            swal({
              title: "Your lesson is updated!",
              icon: "success",
            }).then((value) => {
              browserHistory.push('/mylesson')
            })
          }
        })
        .catch(e =>{ 
          console.log(e);
          this.setState({
            loading: false
          })
        })
    } else {
      apiLesson.createLesson(lesson)
        .then(res => {
          console.log('res create', res);
          this.setState({
            loading: false,
            shouldBlockNavigation:false
          })
          if (res.success) {
            swal({
              title: "Your lesson is created!",
              icon: "success",
            }).then((value) => {
              browserHistory.push('/mylesson');
            })
          }
        })
        .catch(e => {console.log(e);
          this.setState({
            loading: false
          })
        })
    }

  }
  changeSubtitle(key, subtitles) {
    const { keyActive } = this.state;
    let tempSlide = this.state.slideActive;
    const { newLesson } = this.props.location.state;
    let lesson = newLesson ? newLesson : this.props.lesson;

    tempSlide.notes = subtitles;
    // if (tempSlide.endTime <= subtitles[subtitles.length - 1].endTime) {
      tempSlide.endTime = subtitles[subtitles.length - 1].endTime + 1
    // }
    lesson.slides[keyActive - 1] = tempSlide;
    if ((keyActive - 1) < (lesson.slides.length - 1)) {
      for (let index = keyActive; index <= lesson.slides.length - 1; index++) {
        console.log('key active', lesson.slides[index].startTime, "lesson.slides.length", lesson.slides[index - 1].endTime)
        if (lesson.slides[index].startTime <= lesson.slides[index - 1].endTime) {
          // chang time of start of slide
          lesson.slides[index].startTime = lesson.slides[index - 1].endTime;
          // change time of notes
          if (lesson.slides[index].notes[0].startTime <= (lesson.slides[index].startTime)) {
            let durationTime = lesson.slides[index].notes[0].endTime - lesson.slides[index].notes[0].startTime;
            lesson.slides[index].notes[0].startTime = (lesson.slides[index].startTime) + 1;
            lesson.slides[index].notes[0].endTime = lesson.slides[index].notes[0].startTime + durationTime;

            for (let indexNote = 1; indexNote < lesson.slides[index].notes.length; indexNote++) {
              let durationTime = lesson.slides[index].notes[indexNote].endTime - lesson.slides[index].notes[indexNote].startTime;
              if (lesson.slides[index].notes[indexNote].startTime <= lesson.slides[index].notes[indexNote - 1].endTime) {
                lesson.slides[index].notes[indexNote].startTime = lesson.slides[index].notes[indexNote - 1].endTime + 1;
                lesson.slides[index].notes[indexNote].endTime = lesson.slides[index].notes[indexNote].startTime + durationTime;
              }

            }

          }

          // if (lesson.slides[index].endTime <= (lesson.slides[index].notes[lesson.slides[index].notes.length - 1].endTime)) {
            lesson.slides[index].endTime = (lesson.slides[index].notes[lesson.slides[index].notes.length - 1].endTime)
          // }
          console.log('endTime',lesson.slides[index].endTime )
        }

      }
    }
    console.log(' lesson',lesson )
    this.setState({
      lesson: lesson
    })

    store.dispatch(updateLesson(lesson))

  }

  componentWillUnmount(){
    console.log('un mount----')
  }
  render() {
    const { slideActive, keyActive, lesson, loading, showPrivewModal, shouldBlockNavigation } = this.state;
    console.log('shouldBlockNavigation', shouldBlockNavigation);
    
    return (
      <Layout>
        <div className="edit-container">
          <div className="slide">
            <div className="col-left">
              {lesson.slides.map((slide, key) => (
                <SlideImage
                  key={key}
                  onClick={() => this.setState({ slideActive: slide, keyActive: key + 1 })}
                  className="img"
                  src={getUrlFile(slide.srcSlide)}
                  name={`slide ${key + 1}`}
                />
              ))}
            </div>
            <div className="slide-active">
              <div className="slide-image">
                <img src={getUrlFile(slideActive.srcSlide)} className="slide" alt="slide-active" />
                <Button onClick={()=>{this.setState({showPrivewModal: true})}}><Icon style={{fontSize: 20}} type="caret-right"/>Preview</Button>
              </div>

              <SubtitleEditer
                indexActivedSlide={keyActive - 1}
                name={"Slide " + keyActive}
                subtitles={slideActive.notes}
                onChange={(indexActivedSlide,subtitles) => this.changeSubtitle(indexActivedSlide, subtitles)}
                minValueSlide={(keyActive-1)===0 ? 0 : lesson.slides[keyActive-2].endTime}
              />
            </div>
          </div>
          <div onClick={() => this.createLesson()} className="button-create-video" >
            <img src={convertIcon} style={{ height: 50, width: 'auto' }} alt="img" />
            {this.isEdit
              ?
              <span>Save Lesson</span>
              :
              <span>Create Lesson</span>
            }
          </div>
        </div>
        <div className="spin" style={loading?{}:{display:'none'}}>
          <Spin tip="Loading..." size="large"/>
        </div>

        <PreviewLessonModal
          visible={showPrivewModal}
          lesson={lesson}
          onCancel={()=> {this.setState({showPrivewModal: false})}}
          onCreateLesson={()=>{this.setState({showPrivewModal: false}); this.createLesson()}}
        />
        <Prompt
      when={shouldBlockNavigation}
      message='You have unsaved changes, are you sure you want to leave?'
    />
      </Layout>
    );
  }
}

const mapStateTopProps = (state) => ({
  lesson: state.lesson
});

export default connect(mapStateTopProps)(Edit)