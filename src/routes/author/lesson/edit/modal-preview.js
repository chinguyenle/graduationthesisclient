import React, { Component } from 'react'

import { Modal, Spin } from 'antd';

import classnames from 'classnames'
import './modal-preview.scss'
import swal from 'sweetalert';
import { apiLesson } from '../../../../api/api-lesson';
import ButtonBorder from '../../../../components/button/button-border';
import { SlideShow } from '../../../../components/slide/slide-show';


export class PreviewLessonModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      lesson: null,
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      
    const { visible, lesson } = nextProps;
      this.setState({
        visible: visible,
      })
      visible&&this.getLessonPreview(lesson);
    }
  }
  componentDidMount() {
    const { visible, lesson } = this.props;
    this.setState({
      visible: visible,

    })
    visible&&lesson&&this.getLessonPreview(lesson);
  }

  getLessonPreview = (lesson) => {
    
    apiLesson.previewLesson(lesson)
    .then(res => {
      console.log(res)
      if(!res.error){
        this.setState({
          lesson:res.lesson
        })
      }
    })
    .catch(e => console.log(e))
  }

  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      lesson: null
    }, () => onCancel())
  }

  createLesson(){
    const {onCreateLesson} = this.props;
    this.setState({
      visible: false,
      lesson: null
    }, () => onCreateLesson())
  }

  createTempPreview(lesson){
    let templeSlide = lesson;
    for (let indexSlide = 0; indexSlide < lesson.slides.length; indexSlide++) {
      
      for (let indexNote = 0; indexNote < lesson.slides[indexSlide].notes.length; indexNote++) {
        templeSlide.slides[indexSlide].notes[indexNote].id = indexSlide+""+indexNote
        
      }
      
    }
    return templeSlide;
  }

  render() {
    const { visible, lesson } = this.state;
    const {createCreateFuct} = this.props;

    let tempLessonPreview = lesson?this.createTempPreview(lesson):null;
    // console.log(' temp', tempLessonPreview)
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Close"} onClick={() => this.cancel()} />,
          <ButtonBorder content={"Create lesson"} onClick={() => this.createLesson()} />,
        ]}
        className="modal-add-lesson modal-preview-lesson"
        onCancel={() => this.cancel()}
      >

        <div className="lesson-container">
          <div className="lesson-slide">
            {tempLessonPreview ?
              <>
                <div className="title">{tempLessonPreview.name}</div>
                <SlideShow data={
                  {
                    total_Time: tempLessonPreview.slides[tempLessonPreview.slides.length - 1].endTime,
                    slides: tempLessonPreview.slides
                  }
                } />
              </>
              : <Spin />}
          </div>
        </div>

      </Modal>
    );
  }
}
