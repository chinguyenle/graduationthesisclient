import React,  {Component} from 'react'
import arrowDownIcon from '../../../../assets/icon/icon-arrow-down.png'

import './dowload.scss'
import { Support } from '../../../../components/dowload/support';
import { browserHistory } from '../../../../common/history';


export default class Dowload extends Component {
  render(){
    return(
      <div className="download-container">
        <div className="title">your file is available to dowload</div>
        <div className="button-down-load" onClick={() => console.log('dowload')}>
          <img src={arrowDownIcon} alt="download" style={{width: 33, marginRight: 10}}/>
          Dowload video
        </div>
        <div onClick={()=> browserHistory.push('/upload')} className="other">create another...</div>
        <Support/>
      </div>
    );
  }
}