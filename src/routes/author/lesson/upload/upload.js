import React, { Component } from 'react'
import { GraphFileBrowser } from '@microsoft/file-browser'

import './upload.scss'
import { browserHistory } from '../../../../common/history';
import { ChooseFile } from '../../../../components/choose-file/choose-file';
import { INTRODUCE } from '../../../../components/data-fixed';
import Introduce from '../../../../components/introduce/introduce';
import Layout from '../../../../components/layout/layout';
import { apiLesson } from '../../../../api/api-lesson';
import { api } from '../../../../api/api';
import { store } from '../../../../redux/configureStore';
import { updateLesson } from '../../../../redux/actions/lesson-actions';
import swal from 'sweetalert';
import { Spin } from 'antd';


export default class Upload extends Component {
  state = {
    loading: false
  }
  onSuccess(keys) {
    console.log('onsuccess', keys)
  }

  handleSelectedFile(file) {
    // console.log('onsuccess', moment(1559018415571).diff(moment(1559018414240), "seconds", true))
    this.setState({
      loading: true
    })
    apiLesson.initLesson({ pptFile: file })
      .then(async res => {
        this.setState({
          loading: false
        })
        if (res.slides) {
          await store.dispatch(updateLesson(res))

          return browserHistory.push("/edit", { newLesson: res });
        }
        else {
          swal({
            title: "Have error in progress! try again later",
            icon: "error",
          })
          window.location.reload();
        }
      })
      .catch(e => {
        console.log(e);

        this.setState({
          loading: false
        })
      })
  }
  render() {
    const {loading} = this.state;
    console.log('props', this.props)
    return (
      <Layout>
        <div className="upload-container" >
          <ChooseFile
            handleSelectedFile={(file) => this.handleSelectedFile(file)}
          />

          <div className="introduce-container">
            {INTRODUCE.map((introduce, key) => (
              <Introduce
                key={key}
                title={introduce.title}
                description={introduce.description.replace(/\\r\\n/g, "<br />")}
              />
            ))}
          </div>
        </div>
        <div className="spin" style={loading?{}:{display:'none'}}>
          <Spin tip="Loading..." size="large"/>
        </div>
      </Layout>
    );
  }
}