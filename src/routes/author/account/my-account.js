import React, { Component } from 'react'
import { connect } from 'react-redux'
import './my-account.scss'
import moment from 'moment'
import { Form, Input, DatePicker, Icon, Button, Upload, notification } from 'antd';

import swal from 'sweetalert';
import { apiAuth } from '../../../api/api-auth';
import ButtonBorder from '../../../components/button/button-border';
import { browserHistory } from '../../../common/history';
import Layout from '../../../components/layout/layout';
import { formateDate } from '../../../util/util';
import { Avatar } from '../../../components/avatar/avatar';
import { getUrlFile } from '../../../common/funtions';
import { apiUser } from '../../../api/api-user';
import { store } from '../../../redux/configureStore';
import { updateUser } from '../../../redux/actions/user-actions';
import { ROLE } from '../../../constants';

class MyAccountForm extends Component {
  constructor(props) {
    super(props);
    console.log('props', this.props)
    this.state = {
      confirmDirty: false,
      deleteAvatar: false,
      autoCompleteResult: [],
      avatar: null,
      disable: true,
      disablePassword: true,
    };
  }


  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      const { user } = this.props.user;
      if (!err) {
        let params = Object.assign(values, { avatar: this.state.avatar }, {role:null}, {userId:user.id});
        params.dateOfBirth = formateDate(values.dateOfBirth);
        if(!values.newPassword) {
          params.newPassword=""
        }
        console.log("params", params);
        apiUser.editUser(params)
          .then(res => {
            // console.log('res', res)
            if (res.id) {
              swal({
                title: "Your account is updated!",
                icon: "success",
              }).then((value) => {
                console.log('value', value);
                apiUser.getProfile()
                .then(res => {
                  console.log('ress', res);
                  let isTeacher = res.user.roles[0].name===ROLE.ROLE_TEACHER;
                  store.dispatch(updateUser({...res, isTeacher: isTeacher}))
                    this.setState({
                      disable: true,
                      disablePassword: true
                    })
                })
                .catch(e => console.log(e))
                
              })

            } else {
              swal({
                title: 'Creating account is failed',
                text: res.message || 'There was some error during account creation, Please try again!',
                icon: "error",
              })
            }
          })
          .catch(e => {
            console.log('e', e);
            swal({
              title: 'Creating account is failed',
              text: e || 'There was some error during account creation, Please try again!',
              icon: "error",
            })
          })
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    if (value && value.length < 6) {
      callback("Password has minimums 6 characters")
    }
    callback();
  }

  checkCurrentPass = (rule, value, callback) => {
    const {user} = this.props.user
    const form = this.props.form;
    if (value ) {
      apiUser.checkCurrentPass({userId:user.id},{currentPassword: value})
      .then(res => {
        if(!res.success) {
          callback("Current password is wrong")
        }
      })
      .catch(e => console.log(e))
    }
    // callback();
  }

  handleFiles(file) {
    console.log(file)
    if (file) {

      this.setState({
        avatar: file
      })
      var reader = new FileReader();
      reader.onload = function (e) {
        document.getElementById('user-avatar-edit')
          .setAttribute('src', e.target.result);
      };

      reader.readAsDataURL(file);
    }
  }
  render() {
    const { getFieldDecorator, resetFields } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      labelAlign: "left"
    };

    const { avatar, disable, deleteAvatar, disablePassword } = this.state;
    const { user } = this.props.user;

    const { avatarId, createdAt, dateOfBirth, email, name, username } = user;
    return (
      <Layout
        titleContent="My Account"
      >
        <Form onSubmit={this.handleSubmit} className="register-form my-account-form" layout="horizontal"
          {...formItemLayout}
          encType="multipart/form-data"
        >
          <Form.Item
            label="avatar"
            className="avatar-item"
          >
            <div className="avatar-container-item"  >
              {(avatar || avatarId) && !disable && <Icon type="close" className="delete-avatar" onClick={() => this.setState({ avatar: null, deleteAvatar: true })} />}
              <label for="file1">
                {avatar
                  ? <img id="user-avatar-edit" src="" className="user-avatar" />
                  : <Avatar avatar={deleteAvatar
                    ? ""
                    : avatarId
                      ? getUrlFile(avatarId)
                      : ""} className="user-avatar" name={name} />
                }
              </label>
              <input disabled={disable} type="file" id="file1" style={{ display: "none" }} accept="image/*" name="image" onChange={(e) => this.handleFiles(e.target.files[0])} />
            </div>
          </Form.Item>
          <Form.Item
            label="username"
          >
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
              initialValue: username
            })(
              <Input placeholder="Username" disabled />
            )}
          </Form.Item>
          <Form.Item
            label="Email"
            hasFeedback={!disable}
          >
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: 'The input is not valid E-mail!', },
                { required: true, message: 'Please input your email!' }],

              initialValue: email
            })(
              <Input placeholder="Email" disabled={disable} />
            )}
          </Form.Item>
          <Form.Item
            label="name"
            hasFeedback={!disable}
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name!' }],
              initialValue: name
            })(
              <Input placeholder="name" disabled={disable} />
            )}
          </Form.Item>
          <Form.Item
            label="Date of birth"
            hasFeedback={!disable}
          >
            {getFieldDecorator('dateOfBirth', {
              rules: [{ required: true, message: 'Please input your date of birth!' }],
              initialValue: moment(dateOfBirth)
            })(

              <DatePicker disabled={disable} />
            )}
          </Form.Item>
          {
            disablePassword
              ? 
              <Form.Item
                label="Password"
                hasFeedback={!disablePassword}
              >
                {getFieldDecorator('password', {
                  rules: [{
                    required: true, message: 'Please input your password!',
                  }, {
                    validator: this.validateToNextPassword,
                  }],
                  initialValue: "dfsdfsdf"
                })(
                  <Input type="password" placeholder="Password" disabled={disablePassword} addonAfter={<Icon type="edit" onClick={() => this.setState({ disablePassword: false })} />} />
                )}
              </Form.Item>
              : <>
                {/* <Form.Item
                  label="Current Password"
                  hasFeedback={!disablePassword}
                >
                  {getFieldDecorator('current-password', {
                    rules: [{
                      required: true, message: 'Please input your password!',
                    }, {
                      validator: this.checkCurrentPass,
                    }],
                  })(
                    <Input type="password" placeholder="Password" disabled={disablePassword} addonAfter={<Icon type="edit" onClick={() => this.setState({ disablePassword: false })} />} />
                  )}
                </Form.Item> */}
                <Form.Item
                  label="New password"
                  hasFeedback={!disablePassword}
                >
                  {getFieldDecorator('newPassword', {
                    rules: [{
                      required: true, message: 'Please confirm your password!',
                    }, {
                      validator: this.validateToNextPassword,
                    }],
                  })(
                  <Input type="password" placeholder="New Password" onBlur={this.handleConfirmBlur} addonAfter={<Icon type="edit" onClick={() => this.setState({ disablePassword: true })}/>} />
                  )}
                </Form.Item>
                <Form.Item
                  label="Confirm password"
                  hasFeedback={!disablePassword}
                >
                  {getFieldDecorator('confirm-newPassword', {
                    rules: [{
                      required: true, message: 'Please confirm your password!',
                    }, {
                      validator: this.compareToFirstPassword,
                    }],

                  })(
                    <Input type="password" placeholder="Confirm New Password" onBlur={this.handleConfirmBlur} />
                  )}
                </Form.Item>
              </>
          }
          {
            !disable || !disablePassword
              ?
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <ButtonBorder
                  content={"Cancel"}
                  // type="submit"
                  onClick={() => this.setState({ disable: true, disablePassword: true }, resetFields())}
                  className={"my-account-bottom-button"}
                />
                <ButtonBorder
                  content={"Save"}
                  type="submit"
                  className={"my-account-bottom-button"}
                />
              </div>
              :
              <ButtonBorder
                content={"Edit"}
                // type="submit"
                onClick={() => this.setState({ disable: false })}
              />
          }
        </Form>

      </Layout>
    );
  }
}
const MyAccount = Form.create({ name: 'MyAccount' })(MyAccountForm);
const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(MyAccount)