import React, { Component } from 'react'

import Layout from '../../components/layout/layout'
import './new-class.scss'
import { Form, Input, Icon, Button, Avatar, Upload, notification } from 'antd';
import ButtonBorder from '../../components/button/button-border';
import { apiAuth } from '../../api/api-auth';
import { browserHistory } from '../../common/history';
import swal from 'sweetalert';
import { apiClass } from '../../api/api-class';

export class NewClassForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    avatar: null
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let params = Object.assign(values, { avatar: this.state.avatar });
        apiClass.createClass(params)
        .then(res => {
          console.log('res', res)
            swal({
              title: "Your account is created!",
              icon: "success",
            }).then((value) => {
              console.log('value', value);
              browserHistory.push('/myclass');
            })
        })
        .catch(e => {
          console.log('e', e)
        })
      }
    });
  }

  handleFiles(file) {
    console.log(file)
    if (file) {

      this.setState({
        avatar: file
      })
      var reader = new FileReader();
      reader.onload = function (e) {
        document.getElementById('class-avatar')
          .setAttribute('src', e.target.result);
      };

      reader.readAsDataURL(file);
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      labelAlign: "left"
    };
    const {avatar} = this.state;
    return (
      <Layout
        titleContent="New class"
      >
        <Form onSubmit={this.handleSubmit} className="register-form new-class" layout="horizontal"
          {...formItemLayout}
        >
        <Form.Item
          label="class avatar"
          className="avatar-item"
        >
        <div className="avatar-container-item"  >
            {avatar && <Icon type="close" className="delete-avatar" onClick={() => this.setState({ avatar: null })} />}
            <label for="file1">
              {avatar
                ? <img id="class-avatar" src="" className="user-avatar" />
                : <Avatar size={64} icon="user" />
              }
            </label>
            <input type="file" id="file1" style={{ display: "none" }} accept="image/*" name="image" onChange={(e) => this.handleFiles(e.target.files[0])} />
          </div>
        </Form.Item>
          <Form.Item
            label="class Name"
          >
            {getFieldDecorator('className', {
              rules: [{ required: true, message: 'Please input your class name!' }],
            })(
              <Input placeholder="Class Name" />
            )}
          </Form.Item>
          <ButtonBorder
            content={"Create Class"}
            type="submit"
          />
        </Form>

      </Layout>
    );
  }
}
export const NewClass = Form.create({ name: 'newClass' })(NewClassForm);