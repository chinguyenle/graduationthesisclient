import React, { Component } from 'react'
import Layout from '../../../components/layout/layout';
import { browserHistory } from '../../../common/history';
import { apiLesson } from '../../../api/api-lesson';
import { connect } from 'react-redux';
import LessonCard from '../../../components/class/lesson-card';
import ButtonBorder from '../../../components/button/button-border';
import { Popover, Button, Spin } from 'antd';


class MyLesson extends Component {
  constructor(props) {
    super(props);
    console.log('props', props)
    this.state = {
      loading: true,
      myLesson: []
    }
  }
  componentDidMount() {
    this.getData()
  }

  getData() {
    const { user } = this.props;
    this.setState({
      loading: true
    })
    apiLesson.getLessonByUser(user.id)
      .then(res => {
        console.log('my Class', res);
        if (res.size > 0) {
          this.setState({
            myLesson: res.content
          })
        }
        this.setState({
          loading: false
        })
      })
      .catch(e => { console.log(e); this.setState({ loading: false }) })

  }
  editLesson() {

  }
  deleteLesson() {

  }

  render() {
    let { myLesson, loading } = this.state;

    return (
      <Layout
        titleContent="my lesson"
      >
        {!loading ?
          myLesson.length > 0 ?
            myLesson.map(item => (
              <LessonCard
                lessonInfo={item}
                onClick={() => browserHistory.push(`/mylesson/lesson=${item.name}`, { lessonId: item.id })}
                refreshData={()=>this.getData()}
              />
            ))
            : <div>
              {"You haven't any lesson"}
            </div>
          : <Spin />
        }
      </Layout>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(MyLesson)
