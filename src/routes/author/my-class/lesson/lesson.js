import React, { Component } from 'react'
import { connect } from 'react-redux'

import Layout from '../../../../components/layout/layout';
import { Spin } from 'antd';

import './lesson.scss';
import { Love, Comment } from '../../../../components/slide/reaction/reaction';
import { SlideShow } from '../../../../components/slide/slide-show';
import Comments from '../../../../components/comment/comments';
import { TestData } from '../../../../common/test-data';
import { apiLesson } from '../../../../api/api-lesson';
import { stat } from 'fs';


class Lesson extends Component {
  constructor(props) {
    super(props);
    console.log('props', props);

    this.state = {
      lesson: null,
      createdBy: null,
      comments: null
    }
  }

  componentDidMount() {
    const { state } = this.props.location;
    const lessonId = state ? state.lessonId : null;
    if (lessonId) {
      this.getLessonDetail(lessonId);
    }
  }
  getLessonDetail(lessonId) {
    apiLesson.getLessonDetail(lessonId)
      .then(res => {
        console.log('res', res)
        this.setState({
          lesson: res.lesson,
          createdBy: res.createdBy,
          comments: res.comments
        })
      })
      .catch(e => console.log(e))
  }
  likeHandle(like) {
    const { user } = this.props;
    const { state } = this.props.location;
    const lessonId = state ? state.lessonId : null;
    let params = { userId: user.id, lessonId: lessonId };
    if (like) {
      apiLesson.like(params)
        .then(res => { console.log(res) })
        .catch(e => console.log(e))
    } else {
      apiLesson.dislike(params)
        .then(res => { console.log(res) })
        .catch(e => console.log(e))
    }
  }
  activeLike() {
    const { user } = this.props;
    const { state } = this.props.location;
    const { lesson } = this.state;
    let activeLike = false;
    if (lesson) {
      lesson.users.map(item => {
        if (item.id === user.id) activeLike = true;
      })
    }
    return activeLike
  }
  postComment(text, callBack) {
    const { user } = this.props;
    const { state } = this.props.location;
    const lessonId = state ? state.lessonId : null;
    let params = { userId: user.id, lessonId: lessonId, text: text };
    if (text.length > 0) {
      apiLesson.postComment(params)
        .then(res => {
          console.log(res)
          if (res.success) {
            this.setState({
              comments: res.comments
            });
            callBack();
          }
        })
        .catch(e => console.log(e))
    }
  }
  getComment() {
    const { state } = this.props.location;
    const lessonId = state ? state.lessonId : null;
    apiLesson.getComment({ lessonId: lessonId })
      .then(res => {
        console.log('get comment')
        if (res.success) {
          console.log('ge ', res)
          this.setState({
            comments: res.comments
          })
        }
      })
      .catch(e => console.log(e))
  }
  render() {
    let { title, number, user } = this.props;
    let { lesson, createdBy, comments } = this.state;
    title = lesson ? lesson.name : "";
    let activeLike = this.activeLike();
    // console.log("lesson", lesson);
    return (
      <Layout>
        <div className="lesson-container">
          <div className="lesson-slide">
            {lesson ?
              <>
                <div className="title">{title}</div>
                <SlideShow data={
                  {
                    total_Time: lesson.slides[lesson.slides.length - 1].endTime,
                    slides: lesson.slides
                  }
                } />
                <div className="lesson-reaction">
                  <Love active={activeLike} number={lesson.users.length} onClick={(like) => { this.likeHandle(like); console.log('love', like) }} /> <Comment number={comments.length} href="#comment" />
                </div>
              </>
              : <Spin />}
          </div>
          <div className="device" />
          <div className="author">
            {
              createdBy ?
                <>
                  <div><span className="title">Author: </span>{createdBy ? createdBy.name : "Nguyen Van Lam"}</div>
                  <div><span className="title">Description: </span>{"Giảng viên trường đại học ABC"}</div>
                </>
                : <Spin />
            }
          </div>
          <div className="device larger" />
        </div>
        <Comments
          id={"comment"}
          comments={comments}
          postComment={(text, callBack) => this.postComment(text, callBack)}
          getComment={() => this.getComment()}
          user={user}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps)(Lesson)