import React, { Component } from 'react'

import { Modal, notification } from 'antd';

import classnames from 'classnames'
import './modal-join-request.scss'
import swal from 'sweetalert';
import { apiClass } from '../../../api/api-class';
import ButtonBorder from '../../../components/button/button-border';


export class JoinRequestModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      lessons: null,
      listRequest: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        visible: nextProps.visible,
        listRequest: nextProps.listJoinRequest

      })
    }
  }
  componentDidMount() {
    const { visible } = this.props;
    this.setState({
      visible: visible
    })
  }

  acceptRequest = (requestId, classId, userId) => {
    let params = {
      requestId,
      classId,
      userId
    }
    apiClass.acceptRequest(params)
      .then(res => {
        console.log(res);
        if (res.success) {
          notification.success({
            description: "Student is added",
            duration:2
          })
          
        this.removeRequest(requestId)
        }
      })
      .catch(e => console.log(e))
  }
  cancelRequest = (requestId) => {
    apiClass.cancelRequest({ requestId: requestId })
    .then(res => {
      console.log(res);
      if (res.success) {
        notification.success({
          description: "Request is canceled",
          duration:2
        })
        this.removeRequest(requestId)
      }
    })
      .catch(e => console.log(e))
  }
  removeRequest(requestId) {
    let {listRequest} = this.state;
    let index = listRequest.findIndex(item => item.id === requestId);
    if(index !== -1) {
      listRequest.splice(index, 1)
    }
    this.setState({
      listRequest: listRequest
    })
  }
  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      selectedLesson: null
    }, () => onCancel())
  }


  render() {
    const { visible, listRequest } = this.state;
    console.log("props modal", this.state)
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Close"} onClick={() => this.cancel()} />,
        ]}
        className="modal-add-lesson modal-join-request"
        onCancel={() => this.cancel()}
      >
        <div className="add-lesson-container">
          <div className="add-lesson-content">

            {listRequest && listRequest.map(item =>
              <div className="request-item">
                    <span>{item.userResquest.name+" - "+item.userResquest.email} </span>
                    <ButtonBorder content={"Cancel"} onClick={() => this.cancelRequest(item.id)} />
                    <ButtonBorder content={"Accept"} onClick={() => this.acceptRequest(item.id,item.classId, item.userResquest.id )} />

              </div>
            )} 

          </div>
        </div>
      </Modal>
    );
  }
}
