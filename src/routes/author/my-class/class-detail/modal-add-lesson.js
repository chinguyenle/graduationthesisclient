import React, { Component } from 'react'

import { Modal, notification } from 'antd';

import classnames from 'classnames'
import './modal-add-lesson.scss'
import { apiLesson } from '../../../../api/api-lesson';
import ButtonBorder from '../../../../components/button/button-border';
import LessonCard from '../../../../components/class/lesson-card';
import { apiClass } from '../../../../api/api-class';
import swal from 'sweetalert';


export class AddLessonModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      lessons: null,
      selectedLesson: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        visible: nextProps.visible
      })
      this.getLessonNotInClass()
    }
  }
  componentDidMount() {
    const { visible } = this.props;
    this.setState({
      visible: visible
    })
    this.getLessonNotInClass()
  }

  getLessonNotInClass = () => {
    const { classId } = this.props;
    if (classId) {
      apiLesson.getLessonNotInClass(classId)
        .then(res => {
          this.setState({
            lessons: res.content
          })
          if (res.size > 0) {
            this.setState({
              lessons: res.content
            })
          }
        })
        .catch(e => console.log(e))
    }
  }
  addLessonToClass() {
    const { onConfirm } = this.props;
    const { classId } = this.props;
    const { selectedLesson } = this.state;
    if (selectedLesson) {
      apiClass.addLessonToClass({ classId: classId, lessonId: selectedLesson.id })
        .then(res => {
          if (res.success) {
            swal({
              title: "lesson is added into this class!",
              icon: "success",
            }).then((value) => {
              this.setState({
                visible: false,
                selectedLesson: null
              }, () => onConfirm())
            })
            
          }
        })
        .catch(e => console.log(e))
    }else  notification.warning({
      message: 'HusConvert',
      description: 'You must choose a lesson to confirm'
    });
  }
  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      selectedLesson: null
    }, () => onCancel())
  }


  render() {
    const { lessons, visible, selectedLesson } = this.state;
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Cancel"} onClick={() => this.cancel()} />,
          <ButtonBorder content={"Confirm"} onClick={() => this.addLessonToClass()} />
        ]}
        className="modal-add-lesson"
        onCancel={() => this.cancel()}
      >
        <div className="add-lesson-container">
          <div className="add-lesson-content">

            {lessons && lessons.map(item =>
              <LessonCard
                className={classnames("modal-lesson-card", { "card-actived": selectedLesson ? item.id === selectedLesson.id : false })}
                lessonInfo={item}
                onClick={() => { this.setState({ selectedLesson: item }) }}
              />
            )}

          </div>
        </div>
      </Modal>
    );
  }
}
