import React, { Component } from 'react'
import swal from 'sweetalert';
import _ from "lodash";
import { connect } from 'react-redux'

import './class-detail.scss'
import Layout from '../../../../components/layout/layout';
import { browserHistory } from '../../../../common/history';
import { apiClass } from '../../../../api/api-class';
import ButtonBorder from '../../../../components/button/button-border';
import { SelectUser } from '../../../../components/select-group/select-user';
import { AddLessonModal } from './modal-add-lesson';
import LessonCardForClass from '../../../../components/class/lesson-card-forClass';
import { AddNotificationModal } from './modal-add-notification';
import { Notification } from '../../../../components/notification/notification';



var stompClient = null;
class ClassDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalAddLesson: false,
      showModalAddNotification: false,
      classDetail: null,
      userNotInClass: [],
      classDetail: null,
      listNoti: null,

      // socket
      username: '',
      channelConnected: false,
      chatMessage: '',
      error: '',
      // socket
    }
  }

  // socket
  connect = (userName) => {

    if (userName) {

      this.setState({
        username: userName
      })

      const Stomp = require('stompjs')

      var SockJS = require('sockjs-client')

      SockJS = new SockJS('http://localhost:5000/ws')

      stompClient = Stomp.over(SockJS);

      stompClient.connect({}, this.onConnected, this.onError);

    }
  }

  onConnected = () => {

    // this.setState({
    //   channelConnected: true
    // })

    // Subscribing to the public topic
    stompClient.subscribe('/topic/public', this.onMessageReceived);

    // Registering user to server
    stompClient.send("/app/addUser",
      {},
      JSON.stringify({ sender: this.state.username, type: 'JOIN' })
    )
  }

  onError = (error) => {
    console.log('errrorr notification', error)
    // this.setState({
    //   error: 'Could not connect you to the Chat Room Server. Please refresh this page and try again!'
    // })
  }

  sendMessage = (type, value) => {

    if (stompClient) {
      var chatMessage = {
        sender: this.state.username,
        content: type === 'TYPING' ? value : value,
        type: type,
        localType: 'NOTIFICATION'
      };

      stompClient.send("/app/sendMessage", {}, JSON.stringify(chatMessage));

      // clear message text box after sending the message

    }
  }

  onMessageReceived = (payload) => {

    var message = JSON.parse(payload.body);

    console.log('messageee ', message);
    if (message.type === 'JOIN') {
      // console.log("join")
    }
    else if (message.type === 'CHAT'&&message.localType==='NOTIFICATION') {

      this.getNotification();
    }
    else if (message.type === 'LEAVE') {
      // console.log("leave")
    }
    else if (message.type === 'TYPING') {
      // console.log("typing")
    }
    else {
      // console.log("do nothing")
      // do nothing...
    }
  }

  componentDidMount() {   
     const { user } = this.props
    this.connect(user.user.username)
    this.getClassData();
    this.getNotification();
  }

  getClassData() {
    const { match } = this.props;
    const { classId } = match.params
    apiClass.getClassById(classId)
      .then(res => {
        this.setState({
          classDetail: res.classRoom
        })
      })
      .catch(e => console.log(e))
    this.getUserNotInClass();

  }
  getNotification() {
    const { match } = this.props;
    const { classId } = match.params
    apiClass.getNotification(classId)
      .then(res => {
        console.log('res', res)
        if (res.size > 0) {
          this.setState({
            listNoti: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }
  getUserNotInClass() {
    const { match } = this.props;
    const { classId } = match.params
    apiClass.getUserNotInClass(classId)
      .then(res => {
        if (res.size > 0) {
          this.setState({
            userNotInClass: res.content
          })
        }
      })
      .catch(e => console.log(e))
  }
  addUserToClass(listIndex, callBack) {
    const { match } = this.props;
    const { classId } = match.params
    swal({
      title: "Add new student",
      icon: "warning",
      buttons: true,
    }).then((value) => {
      if (value) {
        let { userNotInClass } = this.state;
        let user = [];
        listIndex.map(item => {
          let userTemp = _.find(userNotInClass, user => user.id === Number(item));
          if (userTemp)
            user.push(userTemp)
        })
        // console.log("user  add on", user, classId)
        apiClass.addUserToClass(classId, user)
          .then(res => {
            console.log("ress", res);
            this.setState({
              classDetail: res.classRoom
            })
            this.getUserNotInClass(); callBack()
            swal({
              title: "Success",
              icon: "success",
            })
          })
          .catch(e => console.log('e', e))
      }
    })

  }
  deleteUser(listIndex, callBack) {
    const { match } = this.props;
    const { classId } = match.params;
    const { classDetail } = this.state;
    swal({
      title: "Delete student?",
      icon: "warning",
      buttons: true,
    }).then((value) => {
      if (value) {
        let user = [];
        listIndex.map(item => {
          let userTemp = _.find(classDetail.users, user => user.id === Number(item));
          if (userTemp)
            user.push(userTemp)

        })
        apiClass.deleteUser(classId, user)
          .then(res => {
            if (res.classRoom) {
              swal({
                title: "Students have been deleted",
                icon: "success",
              })
              this.setState({
                classDetail: res.classRoom
              })
              this.getUserNotInClass(); callBack()
            }
          })

          .catch(e => console.log('e', e))
      }
    })

  }

  handleAddLesson() {
    this.setState({
      showModalAddLesson: false
    })
    this.getClassData()
  }

  handleCancelAddLesson() {
    this.setState({
      showModalAddLesson: false
    })
  }

  handleAddNotification() {
    this.setState({
      showModalAddNotification: false
    })
    this.getNotification()
  }
  handleCancelAddNotification() {
    this.setState({
      showModalAddNotification: false
    })
  }
  render() {
    let { classDetail, userNotInClass, showModalAddLesson, showModalAddNotification, listNoti } = this.state;
    const { location, match, user } = this.props
    const { classId } = match.params;
    const { classInfo } = location.state;
    const { classRoomName } = classInfo;
    return (
      <Layout
        titleContent={classRoomName}
        className="class-detail"
        rightContent={
          user.isTeacher ?
            <div className="layout-extend-right">

              <SelectUser
                listUsers={userNotInClass}
                onAdd={(listIndex, callBack) => this.addUserToClass(listIndex, callBack)}
              />
              <SelectUser
                title="Delete student"
                buttonTitle="delete"
                listUsers={classDetail ? classDetail.users : []}
                onAdd={(listIndex, callBack) => this.deleteUser(listIndex, callBack)}
              />
              <ButtonBorder
                content={"Add more lesson"}
                onClick={() => this.setState({ showModalAddLesson: true })}
              />

            </div>
            : null
        }
        leftContent={
          <div className="layout-extend-left">
            {
              user.isTeacher ?
                <ButtonBorder
                  content={"Add Notification"}
                  onClick={() => this.setState({ showModalAddNotification: true })}
                  className={"add-noti-button"}
                /> :
                <div className="title-noti">
                  Notification
              </div>}
            <div className="list-noti">
              {listNoti && listNoti.map(noti => (
                <Notification
                  key={noti.id}
                  name={noti.createdBy}
                  createdAt={noti.createdAt}
                  content={noti.text}
                />
              ))}
            </div>
          </div>
        }
      >
        <div className="list-lessons">

          {classDetail
            ?
            classDetail.lessons.length > 0
              ?
              classDetail.lessons.map(item => (
                <LessonCardForClass
                  lessonInfo={item}
                  onClick={() => { browserHistory.push(`${location.pathname}/lesson=:${item.name}`, { lessonId: item.id }) }}
                />
              ))
              : <div style={{ width: 480, backgroundColor: '#fff' }}>
                {"Don't have any lessons"}
              </div>
            : <div style={{ width: 480, backgroundColor: '#fff' }}>
              {"Don't have any lessons"}
            </div>
          }
        </div>
        <AddLessonModal
          classId={classId}
          visible={showModalAddLesson}
          onConfirm={() => this.handleAddLesson()}
          onCancel={() => this.handleCancelAddLesson()}
        />
        <AddNotificationModal
          classId={classId}
          user={user}
          visible={showModalAddNotification}
          onConfirm={() => {this.handleAddNotification(); this.sendMessage('CHAT', 'thông báo')}}
          onCancel={() => this.handleCancelAddNotification()}
        />
      </Layout>

    );
  }
}

const mapStatetoProps = (state) => ({
  user: state.user
})

export default connect(mapStatetoProps)(ClassDetail);