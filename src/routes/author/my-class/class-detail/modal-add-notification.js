import React, { Component } from 'react'
import _ from 'lodash'
import { Modal, notification } from 'antd';

import classnames from 'classnames'
import './modal-add-notification.scss'
import ButtonBorder from '../../../../components/button/button-border';
import { apiClass } from '../../../../api/api-class';
import swal from 'sweetalert';


export class AddNotificationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible:false,
      text:""
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        visible: nextProps.visible
      })
    }
  }
  componentDidMount() {
    const { visible } = this.props;
    this.setState({
      visible: visible
    })
  }

  sendNotification() {
    const { onConfirm, user } = this.props;
    const { classId } = this.props;
    const { text } = this.state;
    if (!_.isEmpty(text.trim())) {
      apiClass.createNotification({ classId: classId, userId: user.id, text: text.trim() })
        .then(res => {
          if (res.success) {
            swal({
              title: "Notification is sent into this class!",
              icon: "success",
            }).then((value) => {
              this.setState({
                visible: false,
                selectedLesson: null
              }, () => onConfirm())
            })
            
          }
        })
        .catch(e => console.log(e))
    }else  notification.warning({
      message: 'HusConvert',
      description: 'You must write something'
    });
  }
  cancel() {
    const { onCancel } = this.props;
    this.setState({
      visible: false,
      text: ""
    }, () => onCancel())
  }

  onChange(e){
    this.setState({
      text: e.target.value
    })
  }

  render() {
    const { text, visible, selectedLesson } = this.state;
    return (
      <Modal
        visible={visible}
        footer={[
          <ButtonBorder content={"Cancel"} onClick={() => this.cancel()} />,
          <ButtonBorder content={"Send"} onClick={() => this.sendNotification()} />
        ]}
        className="modal-add-lesson modal-notification"
        onCancel={() => this.cancel()}
        centered
      >
        <div className="notification-container">

          <textarea rows={5} placeholder={"enter your notification"} value={text} style={{width:'100%', minHeight: 300}} className="text-area" onChange={(e)=> this.onChange(e)} >

          </textarea>
        </div>
      </Modal>
    );
  }
}
