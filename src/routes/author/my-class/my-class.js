import React, { Component } from 'react'
import Layout from '../../../components/layout/layout';
import ClassCard from '../../../components/class/class-card';
import { browserHistory } from '../../../common/history';
import { apiClass } from '../../../api/api-class';
import { JoinRequestModal } from './modal-join-request';


class MyClass extends Component {
  constructor(props){
    super(props);
    this.state={
      showJoinRequestModal: false,
      listJoinRequest:[],
      myClass:[]
    }
  }
  componentDidMount(){
    this.getData()
  }

  getData(){
    apiClass.getMyClasses()
    .then(res => {
      console.log('my Class', res);
      if(res.size > 0){
        this.setState({
          myClass: res.content
        })
      }
    })
    .catch(e => console.log(e))
    
  }
  showListJoinRequest(listJoinRequest){
    console.log("list", listJoinRequest)
    this.setState({
      showJoinRequestModal: true,
      listJoinRequest
    })
  }
  render () {
    let {myClass, showJoinRequestModal, listJoinRequest} = this.state;
    return (
      <Layout
        titleContent="my class"
      >
      { myClass.length>0?
        myClass.map(item =>(
          <>
          <ClassCard
            classInfo={item}
            onClick={()=>browserHistory.push(`/myclass/${item.classRoom.id}&${item.classRoom.classRoomName}`, {classInfo: item.classRoom})}
            showListRequest={(list)=>this.showListJoinRequest(list)}
          />
          </>
        ))
        :<div>
          {"You haven't joined any class yet"}
        </div>
      }
      <JoinRequestModal
        visible={showJoinRequestModal}
        listJoinRequest={listJoinRequest}
        onCancel={()=>{this.setState({showJoinRequestModal: false}); this.getData()}}
      />
      </Layout>
    )
  }
}

export default MyClass