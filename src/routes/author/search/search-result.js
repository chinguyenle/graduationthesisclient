import React, { Component } from 'react'

import { Form, Input, DatePicker, Icon, Button, Upload, notification } from 'antd';
import "./search-result.scss"
import moment from 'moment'
import Layout from '../../../components/layout/layout';
import ClassCard from '../../../components/class/class-card';
import { browserHistory } from '../../../common/history';
import { apiClass } from '../../../api/api-class';
import { apiUser } from '../../../api/api-user';
import { Spin } from 'antd';
import { getUrlFile } from '../../../common/funtions';
import { Avatar } from '../../../components/avatar/avatar';


class SearchResultForm extends Component {
  constructor(props) {
    super(props);
    console.log('this porps', this.props)
    this.state = {
      result: null,
      isSearchTeacher: true
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps != this.props) {
      
    const { params } =nextProps.match;
    const { key } = params;
    this.getData(key)
      // window.location.reload()
    }
  }
  componentDidMount() {
    const { params } = this.props.match;
    const { key } = params;
    this.getData(key)
  }

  getData(key) {
    let keys = key.split("-");
    console.log('keys', keys)
    if (keys[0] === "class") {
      apiClass.getClassById(Number(keys[1]))
        .then(res => {
          console.log('my Class', res);
          this.setState({
            result: res,
            isSearchTeacher: false,
          })

        })
        .catch(e => console.log(e))

    } else {
      apiUser.getProfilerById(Number(keys[1]))
        .then(res => {
          console.log('my user', res);
          this.setState({
            result: res,
            isSearchTeacher: true,
          })

        })
        .catch(e => console.log(e))
    }

  }
  showListJoinRequest(listJoinRequest) {
    console.log("list", listJoinRequest)
    this.setState({
      showJoinRequestModal: true,
      listJoinRequest
    })
  }
  renderUserForm() {
    const { getFieldDecorator } = this.props.form;
    const { user, joinedClasses } = this.state.result;
    const { avatarId, dateOfBirth, email, name, username } = user;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      labelAlign: "left"
    };
    console.log('user', user)
    return (
      <div style={{width: '100%'}}>
        <Form onSubmit={this.handleSubmit} className="register-form my-account-form" layout="horizontal"
          {...formItemLayout}
          encType="multipart/form-data"
        >
          <Form.Item
            label="avatar"
            className="avatar-item"
          >
            <div className="avatar-container-item"  >
              <Avatar avatar={avatarId ? getUrlFile(avatarId) : null} className="user-avatar" name={name} />
            </div>
          </Form.Item>
          <Form.Item
            label="username"
          >
            {getFieldDecorator('username', {
              initialValue: username
            })(
              <Input placeholder="Username" disabled />
            )}
          </Form.Item>
          <Form.Item
            label="Email"
          >
            {getFieldDecorator('email', {

              initialValue: email
            })(
              <Input placeholder="Email" disabled />
            )}
          </Form.Item>
          <Form.Item
            label="name"
          >
            {getFieldDecorator('name', {
              initialValue: name
            })(
              <Input placeholder="name" disabled />
            )}
          </Form.Item>
          <Form.Item
            label="Date of birth"
          >
            {getFieldDecorator('dateOfBirth', {
              initialValue: moment(dateOfBirth)
            })(

              <DatePicker disabled />
            )}
          </Form.Item>

        </Form>
        <div className="list-class-result">
          <div className="title-class-result">Your classes</div>
          {joinedClasses.length > 0
            ? 
              joinedClasses.map(item => (

                <ClassCard
                  classInfo={item}
                  onClick={() => { }}
                // showListRequest={(list) => this.showListJoinRequest(list)}
                />
              ))


         

            : <div>No class</div>}

        </div>

      </div>
    )
  }
  render() {
    let { result, isSearchTeacher } = this.state;

    console.log("state", this.state)
    return (
      <Layout
        titleContent="Result"
      >
        {isSearchTeacher
          ? result
            ?
            this.renderUserForm()

            : <Spin />

          :
          result ?
            <ClassCard
              classInfo={result}
              onClick={() => {}}//browserHistory.push(`/myclass/${result.classRoom.id}&${result.classRoom.classRoomName}`, { classInfo: result.classRoom })}
              showListRequest={(list) => this.showListJoinRequest(list)}
            />
            : <Spin />

        }
      </Layout>
    )
  }
}

const SearchResult = Form.create({ name: 'searchResult' })(SearchResultForm);
export default SearchResult;