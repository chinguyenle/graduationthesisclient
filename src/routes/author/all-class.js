import React, { Component } from 'react'
import Layout from '../../components/layout/layout';
import { browserHistory } from '../../common/history';
import ClassCard from '../../components/class/class-card';
import { apiClass } from '../../api/api-class';
import { Spin } from 'antd';


class AllClass extends Component {
  constructor(props){
    super(props);
    this.state={
      allClass:null
    }
  }
  componentDidMount(){
    this.getData()
  }

  getData(){
    apiClass.getAllClasses()
    .then(res => {
      console.log(res);
      if(res.size > 0){
        this.setState({
          allClass: res.content
        })
      }
    })
    .catch(e => console.log(e))
  }
  render () {
    let {allClass} = this.state;
    console.log('my', allClass)
    return (
      <Layout
        titleContent="All classes"
      >
      {
        allClass?allClass.map(item =>(
          <ClassCard
            classInfo={item}
            onClick={()=>browserHistory.push(`/myclass/${item.classRoom.id}&${item.classRoom.classRoomName}`, {classInfo: item.classRoom})}
            callBack={()=>this.getData()}
          />
        ))
        :<Spin/>
      }
      </Layout>
    )
  }
}

export default AllClass
