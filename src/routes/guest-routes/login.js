import React, { Component } from 'react'

import { store } from '../../redux/configureStore'
import { Form, Input, Icon, notification } from 'antd';

import './login.scss'
import ButtonBorder from '../../components/button/button-border';
import { Modal } from '../../components/modal/modal';
import { browserHistory } from '../../common/history';
import { apiAuth } from '../../api/api-auth';
import { lcStorage } from '../../common/util';
import HusConvert from '../../assets/images/HusConvert.png'
import { apiUser } from '../../api/api-user';
import { updateUser } from '../../redux/actions/user-actions';
import { ROLE } from '../../constants';


const FormItem = Form.Item;
export class LoginForm extends Component {
  constructor(props) {
    super(props);
    console.log('props', props)
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  login = () => {
    Modal.dismiss();
    notification.success({
      message: 'Polling App',
      description: "You're successfully logged in.",
    });
    browserHistory.push('/myclass');
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
       apiAuth.login(values)
          .then( res => {
            console.log('respose', res);
            if (!res.error) {
              lcStorage.setAccessToken(res.accessToken)
              apiUser.getProfile()
              .then(async resProfile => {
                console.log('profile', resProfile);
                let isTeacher = resProfile.user.roles[0].name===ROLE.ROLE_TEACHER;
               await store.dispatch(updateUser({...resProfile, isTeacher: isTeacher}))
                this.login();
            })
              .catch(e => console.log(e))
            }
            else {
              if (res.status === 401) {
                notification.error({
                  message: 'HusConvert',
                  description: 'Your Username or Password is incorrect. Please try again!'
                });
              } else {
                notification.error({
                  message: 'HusConvert',
                  description: res.message || 'Sorry! Something went wrong. Please try again!'
                });
              }
            }
          })

          .catch(error => console.log(error));
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-container">
        <img src={HusConvert} className="logo" alt="img" />
        <div className="device"></div>
        <div className="login">
          <div className="login-title">Login</div>
        </div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator('usernameOrEmail', {
              rules: [{ required: true, message: 'Please input your username or email!' }],
            })(
              <Input
                prefix={<Icon type="user" />}
                size="large"
                name="usernameOrEmail"
                placeholder="Username or Email" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input
                prefix={<Icon type="lock" />}
                size="large"
                name="password"
                type="password"
                placeholder="Password" />
            )}
          </FormItem>
          <div className="bottom">
            <div className="forgot">
              {/* <span onClick={() => browserHistory.push('/lesson')}>forgot password</span>or */}
              <span className="create-account"
                onClick={() => {
                  Modal.dismiss();
                  browserHistory.push('/register')
                }}
              >create account</span></div>

            <ButtonBorder content="OK" type="submit" />
          </div>
        </Form>
      </div>
    );
  }
}


const Login = Form.create({ name: 'login' })(LoginForm);

export const openModalLogin = () => {
  let modal = Modal.show({
    content: (
      <Login />
    ),
    onClose: () => modal.dismiss()
  })
}