import React, { Component } from 'react'

import Layout from '../../components/layout/layout'
import './register.scss'
import { Form, Input, DatePicker, Icon, Button, Avatar, Upload, notification } from 'antd';
import ButtonBorder from '../../components/button/button-border';
import { apiAuth } from '../../api/api-auth';
import { formateDate } from '../../util/util';
import { browserHistory } from '../../common/history';
import swal from 'sweetalert';

export class RegisterForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    avatar: null
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let params = Object.assign(values, { avatar: this.state.avatar });
        params.dateOfBirth = formateDate(values.dateOfBirth);
        apiAuth.signup(params)
          .then(res => {
            console.log('res', res)
            if (res.success == true) {
              swal({
                title: "Your account is created!",
                text: "Please, login to continue",
                icon: "success",
              }).then((value) => {
                console.log('value', value);
                browserHistory.push('/welcome', { openModalLogin: true });
              })

            } else {
              swal({
                title: 'Creating account is failed',
                text: res.message || 'There was some error during account creation, Please try again!',
                icon: "error",
              })
            }
          })
          .catch(e => {
            console.log('e', e);
            swal({
              title: 'Creating account is failed',
              text: e || 'There was some error during account creation, Please try again!',
              icon: "error",
            })
          })
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }
  handleFiles(file) {
    console.log(file)
    if (file) {

      this.setState({
        avatar: file
      })
      var reader = new FileReader();
      reader.onload = function (e) {
        document.getElementById('user-avatar')
          .setAttribute('src', e.target.result);
      };

      reader.readAsDataURL(file);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
      labelAlign: "left"
    };

    const { avatar } = this.state;
    return (
      <Layout
        titleContent="Register"
      >
        <Form onSubmit={this.handleSubmit} className="register-form" layout="horizontal"
          {...formItemLayout}
          encType="multipart/form-data"
        >
          <Form.Item
            label="avatar"
            className="avatar-item"
          >
            <div className="avatar-container-item" >
              {avatar && <Icon type="close" className="delete-avatar" onClick={() => this.setState({ avatar: null })} />}
              <label for="file1">
                {avatar
                  ? <img id="user-avatar" src="" className="user-avatar" />
                  : <Avatar size={64} icon="user" />
                }
              </label>
              <input type="file" id="file1" style={{ display: "none" }} accept="image/*" name="image" onChange={(e) => this.handleFiles(e.target.files[0])} />
            </div>
          </Form.Item>
          <Form.Item
            label="username"
          >
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item
            label="Email"
          >
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: 'The input is not valid E-mail!', },
                { required: true, message: 'Please input your email!' }],
            })(
              <Input placeholder="Email" />
            )}
          </Form.Item>
          <Form.Item
            label="name"
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(
              <Input placeholder="name" />
            )}
          </Form.Item>
          <Form.Item
            label="Date of birth"
          >
            {getFieldDecorator('dateOfBirth', {
              rules: [{ required: true, message: 'Please input your date of birth!' }],
            })(

              <DatePicker />
            )}
          </Form.Item>
          <Form.Item
            label="Password"
          >
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: 'Please input your password!',
              }, {
                validator: this.validateToNextPassword,
              }],
            })(
              <Input type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item
            label="Confirm password"
          >
            {getFieldDecorator('confirm-password', {
              rules: [{
                required: true, message: 'Please confirm your password!',
              }, {
                validator: this.compareToFirstPassword,
              }],
            })(
              <Input type="password" placeholder="Confirm Password" onBlur={this.handleConfirmBlur} />
            )}
          </Form.Item>
          <ButtonBorder
            content={"Register"}
            type="submit"
          />
        </Form>

      </Layout>
    );
  }
}
export const Register = Form.create({ name: 'register' })(RegisterForm);