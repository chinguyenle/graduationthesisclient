import React, { Component } from 'react'
import { connect } from 'react-redux'
import background from '../../../assets/images/welcome.png'
import createSlide from '../../../assets/images/createLesson.png'
import seeSlide from '../../../assets/images/seeLesson.png'
import team from '../../../assets/images/team.png'

import './welcome.scss'
import Layout from '../../../components/layout/layout';
import { openModalLogin } from '../login';
import { ButtonExtent } from '../../../components/header/button-extent';
import { browserHistory } from '../../../common/history';


export class Welcome extends Component {
  componentDidMount() {
    const { state } = this.props.location;
    console.log('prosp', this.props);
    if (state && state.openModalLogin) {
      openModalLogin()
    }
  }
  render() {
    return (
      <Layout
        className="layout-welcome"
        headerRight={
          <div className="right-header-button">
            <div className="right-header-button-view">
              <ButtonExtent name={<a href="#overview">Over view</a>} />
              <ButtonExtent name={<a href="#mission">Mission</a>} />
              <ButtonExtent name={<a href="#weare">About us</a>}/>
            </div>
            <div className="right-header-button-login">
              <ButtonExtent name="Login" className="login" onClick={() => openModalLogin()} />
              <ButtonExtent name="Register" onClick={() => browserHistory.push('/register')} />
            </div>
            </div>
            }
          >
        <div className="step welcome">
              <img src={background} className="image-bg" alt="img" />
              <div className="text">
                <div className="greeting">welcome to the <span>husconvert</span></div>
                <div className="slogan">Simple to create video from slides</div>
              </div>
              <div
                className="button"
                onClick={() => openModalLogin()}>Get Started</div>
            </div>
            <div id="overview" className="step guide">
              <img src={createSlide} className="create-slide" alt="img" />
              <div className="des">
                <div>
                  <div className="title">Create lessons easily</div>
                  <div className="des-detail">UUpload your file simply by dropping them into the box or selecting the "Choose file" option. Add subtitle if you want. And our servers will create video and you just have to download it.
              </div>
                </div>
                <div>
                  <div className="title">Adding a voice</div>
                  <div className="des-detail">A voice will be added automatically based on the subtitles, which you add for each slide. You can choose the type of voice or reading speed.</div>
                </div>
              </div>
            </div> <div id="mission" className="step mission">
              <img src={seeSlide} className="create-slide" alt="img" />
              <div className="des">
                <div className="title">Mission</div>
                <div className="des-detail">Giving you the most convenient and quality products
              </div>
              </div>
            </div>
            <div id="weare" className="step guide">
              <img src={team} className="create-slide" alt="img" />
              <div className="des">
                <div className="title">About us</div>
                <div className="des-detail">{"Director: Nguyen Le Chi"}<br />{"Design: Nguyen Le Chi"}<br />{"Dev team: Nguyen Le Chi"}</div>
              </div>
            </div>
      </Layout>
    );
  }
}


const mapStateToProps = (state) => ({
  state: state,
  user: state.user,
  slide: state.slide
});

export default connect(mapStateToProps)(Welcome)