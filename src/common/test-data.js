export const TestData ={
  total_Time: 132,
  slides:[
    {
      id: 1,
      startTime: 0,
      endTime: 44,
      srcSlide:'http://r.ddmcdn.com/w_830/s_f/o_1/cx_98/cy_0/cw_640/ch_360/APL/uploads/2015/07/cecil-AP463227356214-1000x400.jpg',
      notes:[
        {
          id:'adfdd1',
          text: 'Động vật trên mặt đất là các loài động vật sống chủ yếu hoặc hoàn toàn trên mặt đất như chó, mèo, kiến',
          startTime: 0,
          endTime: 15,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021414.mp3'
        },
        {
          id:'adfdd2',
          text: 'để phân biệt với các loài động vật sống dưới nước (động vật thủy sinh), chủ yếu sống ở dưới nước như tôm, cua hay cá; hoặc dạng hỗn hợp như các loài động vật lưỡng cư như cóc',
          startTime: 16,
          endTime: 28,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021787.mp3'
        },
        {
          id:'adfdd3',
          text: ' Thuật ngữ trên cạn (mặt đất) cũng thường được phân biệt rõ với các loài sống chủ yếu trên mặt đất chứ không phải trên cây',
          startTime: 29,
          endTime: 42,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021418.mp3'
        },

      ]
    },
    {
      id: 2,
      startTime: 45,
      endTime: 110,
      srcSlide:'https://images.mentalfloss.com/sites/default/files/styles/mf_image_16x9/public/178493939.jpg?itok=OFp7umS2&resize=1100x1100',
      notes:[
        {
          id:'adfdd11',
          text: 'Thông thường, người ta chia giới động vật, nghĩa là tổng hợp tất cả các loài động vật thành có xương sống và không xương sống',
          startTime: 46,
          endTime: 70,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021420.mp3'
        },
        {
          id:'adfdd12',
          text: 'Dưới giới, chia thành các: ngành, lớp, bộ, họ, chi, loài, giống. Với một sự đa dạng về kích cỡ, ngoại hình, cấu tạo...',
          startTime: 72,
          endTime: 90,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021423.mp3'
        },
        {
          id:'adfdd13',
          text: 'Rất khó để đưa ra được một con số chính xác về các loài hiện tại còn sống.Vì lẽ các phân loài có khi được xem như loài... lại có nhưng loài mới còn tiếp tục được khám phá và mô tả',
          startTime:95,
          endTime: 110,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021427.mp3'
        },

      ]
    },
    {
      id: 3,
      startTime: 111,
      endTime: 132,
      srcSlide:'https://static.boredpanda.com/blog/wp-content/uploads/2017/01/funny-animal-selfies-fb.png',
      notes:[
        {
          id:'adfdd14',
          text: 'Và trong số đó có rất nhiều loài đã được con người thuần hóa (hoặc tự thuần hóa) để phục vụ nhu cầu của họ.',
          startTime:115,
          endTime: 130,
          srcAudio:'http://s4.bstore.vbee.vn/audio/mp3/15555300236021430.mp3'
        },

      ]
    }
  ],
  comments:[
    {
      user_name: "Lê Thị Tuyết",
      content: 'Bài giảng rất hay, nhiều hình minh học nên rất dễ hiệu, giọng đọc dễ nghe ạ. Nói chung là thầy tuyệt vời! ^^',
      create_at: 1555571217
    },
    {
      user_name: "Nguyễn Văn Hoàng",
      content: 'Nhiều chỗ giọng đọc còn chưa chuẩn ạ. Với cả slide số 10 ý, tại sao nó lại như thế ạ?',
      create_at: 1555571217
    },
    {
      user_name: "Lê Thị Tuyết",
      content: 'Bài giảng rất hay, nhiều hình minh học nên rất dễ hiệu, giọng đọc dễ nghe ạ. Nói chung là thầy tuyệt vời! ^^',
      create_at: 1555571217
    }
  ]
}