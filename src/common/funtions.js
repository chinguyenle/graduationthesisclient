import { browserHistory } from "./history";
import { notification } from "antd";
import { lcStorage } from "./util";
import { removeUser } from "../redux/actions/user-actions";
import { store } from "../redux/configureStore";
import { API_BASE_URL } from "../constants";


export  const logout = async () => {
  // Promise.all(
  //   [
     await lcStorage.removeAccessToken();
    await store.dispatch(removeUser())
    // ])
    // .then(r => {
      notification['success']({
        message: 'Polling App',
        description: "You're successfully logged out."
      });
      browserHistory.push("/");
    // })

}

export const getUrlFile = (fileId) => `${API_BASE_URL}file/fileId=${fileId}`