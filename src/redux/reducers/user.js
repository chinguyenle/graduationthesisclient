
const initState={
    // isTeacher: false
}
export const user=(state={...initState}, action)=> {
    switch (action.type){
        case 'UPDATE_USER': 
            return {
                ...state,
                ...action.user
            };
        case 'REMOVE_USER':
            return {}

        default: return state;
    }
}