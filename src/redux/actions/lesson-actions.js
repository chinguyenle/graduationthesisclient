export const updateLesson = (data) => {
    return {
        type: 'UPDATE_LESSON',
        slide: data
    }
}
export const deleteLesson = ()=> {
    return {
        type: 'DELETE_LESSON'
    }
}